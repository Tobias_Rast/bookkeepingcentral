<!-- Breadcrumb -->
<?php
$crumbs = [
    [
        'page_title' => fetchLine('Earning Rates'),
        'url' => base_url('rate')
    ],
    [
        'page_title' => fetchLine('List of Earning Rates'),
        'url' => base_url('rate')
    ]
];
$this->load->view('inc/breadcrumb', array('crumbs' => $crumbs));

?>
<div class="pcoded-inner-content">  
    <!-- Main-body start -->
    <div class="main-body">
        <div class="page-wrapper">
            <!-- Page-body start -->
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Zero config.table start -->
                        <div class="card">
                          <div class="card-header border-0">
                            <h5><i class="fa fa-group"></i> <?php echo fetchLine('List of Earning Rates'); ?></h5>
                          </div>
                          <div class="card-block">
                            <div class="dt-responsive table-responsive">
                              <table id="base-style" class="table table-striped table-bordered nowrap dataTable" role="grid" aria-describedby="base-style_info">
                                <thead>
                                  <tr>
                                    <th><?php echo fetchLine('Name'); ?></th>
                                    <th><?php echo fetchLine('Type'); ?></th>
                                    <th><?php echo fetchLine('Rate ID'); ?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php if($earning_rates){
                                    foreach($earning_rates as $rate) {
                                      ?>
                                      <tr>
                                        <td><?= $rate->name; ?></td>
                                        <td><?= $rate->earnings_type; ?></td>
                                        <td><?= $rate->earnings_rate_id; ?></td>
                                      </tr>
                                    <?php }
                                  } ?>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        <!-- Zero config.table end -->

                    </div>
                </div>
            </div>
            <!-- Page-body end -->
        </div>
    </div>
    <!-- Main-body end -->
</div>