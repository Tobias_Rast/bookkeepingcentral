<!-- Breadcrumb -->
<div class="page-header">
  <div class="page-block">
    <div class="row align-items-center">
      <div class="col-sm-8">
        <div class="page-header-title">
          <h4 class="m-b-10"><?php echo fetchLine( 'Roles' ); ?></h4>
        </div>
        <ul class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="dashboard">
              <i class="feather icon-home"></i>
            </a>
          </li>
          <li class="breadcrumb-item">
            <a href="users/roles"><?php echo fetchLine( 'Roles' ); ?></a>
          </li>
          <li class="breadcrumb-item">
            <a href="users/roles/addRole"><?php echo fetchLine( 'Add Role' ); ?></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="pcoded-inner-content">
  <!-- Main-body start -->
  <div class="main-body">
    <div class="page-wrapper">
      <!-- Page-body start -->
      <div class="page-body">
        <div class="row">
          <div class="col-sm-12">
            <!-- Basic Form Inputs card start -->
            <div class="card">
              <div class="card-header border-0">
                <h5 class="pb-3"><i class="fa fa-user"></i> <?php echo fetchLine( 'Add Mapping' ); ?></h5>
              </div>
              <div class="card-block">
                
                <form action="<?= site_url('rate/mapping/addMapping'); ?>" method="POST" class="form-material">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group form-primary">
                        <input type="text" class="form-control" name="label" value="<?= set_value('label'); ?>" required="">
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine('Label/Name'); ?></label>
                        </i>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group form-primary">
                        <input type="text" class="form-control" name="table_key" value="<?= set_value('table_key'); ?>" required="">
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine('Table Key'); ?></label>
                        </i>
                      </div>
                    </div>

                     <div class="col-sm-6">
                      <div class="form-group form-primary">
                        <input type="text" class="form-control" name="excel_column" value="<?= set_value('excel_column'); ?>" required="">
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine('Excel Column Header'); ?></label>
                        </i>
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group form-primary select input-wrapper">
                        <select name="type" class="form-control" required="">
                          <option value="text">Text</option>
                          <option value="float">Float/Number</option>
                          <option value="date">Date</option>
                          <option value="email">Email</option>
                        </select>
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine('Data Type'); ?></label>
                      </div>
                    </div>

                    <div class="col-sm-6" >
                      <div class="form-group form-primary select input-wrapper">
                        <select name="is_rate" class="form-control" required="">
                          <option value="0">False</option>
                          <option value="1">True</option>
                        </select>
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine('Is Rate Column?'); ?></label>
                      </div>
                    </div>

                    <div class="col-sm-6" >
                      <div class="form-group form-primary select input-wrapper">
                        <select name="required" class="form-control" required="">
                          <option value="0">False</option>
                          <option value="1">True</option>
                        </select>
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine('Is Value Required?'); ?></label>
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group form-primary select input-wrapper">
                        <select name="rate_id" class="form-control">
                          <option value="">- Select Earning Rate -</option>
                          <?php foreach ($rates as $rate) : ?>
                            <option value="<?= $rate->id ?>"><?= $rate->name ?></option>
                          <?php endforeach; ?>
                        </select>
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine('Rate ID'); ?></label>
                      </div>
                    </div>

                    <div class="col-sm-12 text-right">
                      <hr class="my-3">
                      <button type="submit" class="btn btn-green"><i class="icofont icofont-save"></i> <?php echo fetchLine( 'Save' ); ?></button>
                    </div>
                  </div>
                </form>
                <!-- Basic Form Inputs card end -->

              </div>

              <!-- Main-body end -->

            </div>
          </div>
        </div>

      </div>
      <!-- Page-body end -->
    </div>
  </div>
  <!-- Main-body end -->

</div>
