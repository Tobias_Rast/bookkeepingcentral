<!-- Breadcrumb -->
<?php
$crumbs = [
    [
        'page_title' => fetchLine('Earning Rates'),
        'url' => base_url('rate')
    ],
    [
        'page_title' => fetchLine('List of Earning Rates'),
        'url' => base_url('rate')
    ]
];
$this->load->view('inc/breadcrumb', array('crumbs' => $crumbs));

?>
<div class="pcoded-inner-content">  
    <!-- Main-body start -->
    <div class="main-body">
        <div class="page-wrapper">
            <!-- Page-body start -->
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Zero config.table start -->
                        <div class="card">
                          <div class="card-header border-0">
                            <h5><i class="fa fa-group"></i> <?php echo fetchLine('Earning Rates & Excel Mapping'); ?></h5>
                            <span class="d-inline-block pull-right">
                              <a class="btn btn-info" href="<?= site_url('rate/mapping/addMapping'); ?>"><i class="icofont icofont-plus"></i> <?php echo fetchLine( 'Add Mapping' ); ?></a>
                            </span>
                          </div>
                          <div class="card-block">
                            <div class="dt-responsive table-responsive">
                              <table id="base-style" class="table table-striped table-bordered nowrap dataTable" role="grid" aria-describedby="base-style_info">
                                <thead>
                                  <tr>
                                    <th><?php echo fetchLine('Name'); ?></th>
                                    <th><?php echo fetchLine('Table Key'); ?></th>
                                    <th><?php echo fetchLine('Excel Column Header'); ?></th>
                                    <th><?php echo fetchLine('Validation Type'); ?></th>
                                    <th><?php echo fetchLine('Rate Column'); ?></th>
                                    <th><?php echo fetchLine('Rate ID'); ?></th>
                                    <th><?php echo fetchLine('Action'); ?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php if($items){
                                    foreach($items as $item) {
                                      ?>
                                      <tr>
                                        <td><?= $item->label; ?></td>
                                        <td><?= $item->table_key; ?></td>
                                        <td><?= $item->excel_column; ?></td>
                                        <td><?= $item->type; ?></td>
                                        <td><?= $item->is_rate ? 'True' : 'False' ; ?></td>
                                        <td><?= $item->rate_id; ?></td>
                                        <td>
                                          <a href="<?=site_url('rate/mapping/editMapping/' . $item->id)?>" title="Edit role" class="btn btn-mini btn-info"><i class="icofont icofont-ui-edit p-r-3"></i><?php echo fetchLine( 'Edit' ); ?></a>
                                        </td>
                                      </tr>
                                    <?php }
                                  } ?>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        <!-- Zero config.table end -->

                    </div>
                </div>
            </div>
            <!-- Page-body end -->
        </div>
    </div>
    <!-- Main-body end -->
</div>