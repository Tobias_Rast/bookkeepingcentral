<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class RateModel extends CI_Model {

    public function getEarningRates(){
        $returns = $this->db->get('earning_rates')->result();
        return $returns;
    }

    public function getEarningRateIds() {
        $this->db->select('id,name');
        $query = $this->db->get('earning_rates');
        $result = $query->result();
        return $result;
    }

    
}
