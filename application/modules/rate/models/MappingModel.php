<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MappingModel extends CI_Model {

    public function getMappings(){
        $returns = $this->db->get('mappings')->result();
        return $returns;
    }

    public function getMapping($id) {
        $this->db->select()->from('mappings');
        $this->db->where('id', $id);
        $return = $this->db->get()->row();
        return $return;
    }
    
    public function addMapping($data) {
    	$item = array(
    		'label' => $data['label'],
    		'table_key' => $data['table_key'],
    		'excel_column' => $data['excel_column'],
    		'type' => $data['type'],
    		'required' => $data['required'],
    		'is_rate' => $data['is_rate'],
    		'rate_id' => $data['rate_id']
    	);

		if ($this->db->insert('mappings', $item)) {
            return true;
        }
        return false;
    }

    public function updateMapping($data, $id) {
        $item = array(
    		'label' => $data['label'],
    		'table_key' => $data['table_key'],
    		'excel_column' => $data['excel_column'],
    		'type' => $data['type'],
    		'required' => $data['required'],
    		'is_rate' => $data['is_rate'],
    		'rate_id' => $data['rate_id']
    	);

    	if ($this->db->update('mappings', $item, array('id' => $id))) {
            return true;
        }
        return false;
    }


    
}
