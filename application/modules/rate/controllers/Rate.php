<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use XeroPHP\Application\PrivateApplication;

class Rate extends Base_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('RateModel', 'rate');
    }

    public function index() {
        $data['page'] = 'rate/index';

        $data['earning_rates'] = $this->rate->getEarningRates();

        $this->load->view('template_admin', $data);
    }
}
