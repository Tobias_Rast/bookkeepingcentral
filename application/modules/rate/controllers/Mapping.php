<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use XeroPHP\Application\PrivateApplication;

class Mapping extends Base_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('MappingModel', 'mapping', true);
        $this->load->model('rate/RateModel', 'rate');
    }

    public function index() {
        $data['page'] = 'rate/mapping_list';
        $data['items'] = $this->mapping->getMappings();
        $this->load->view('template_admin', $data);
    }

    private function getDataTypes() {
    	return [
    		'text' => 'Text',
    		'float' => 'Number/Float',
    		'email' => 'Email Address',
    		'date' => 'Date'
    	];
    }

    public function addMapping() { 
    	$this->form_validation->set_rules('label', 'Label', 'required');

    	// @todo, more validation here

        if ($this->form_validation->run() == true) {
            if ($this->mapping->addMapping($this->input->post())) {
                $this->session->set_flashdata('success', 'Mapping Added Successfully !');
                redirect('rate/mapping', 'refresh');
            }
        } elseif ($this->input->post()) {
            $this->session->set_flashdata('danger', stripErrors(validation_errors()));
            redirect($_SERVER['HTTP_REFERER']);
        }else {
        	$rates = $this->rate->getEarningRateIds();
            $data = array('page' => 'rate/mapping_add', 'rates' => $rates);
            $this->load->view('template_admin', $data);
        }
    }

    public function editMapping($id) {
        if ($this->mapping->getMapping($id)) {

        	$this->form_validation->set_rules('label', 'Label', 'required');
        	
        	// @todo, more validation here

            if ($this->form_validation->run() == true) {
                if ($this->mapping->updateMapping($this->input->post(), $id)) {
                    $this->session->set_flashdata('success', 'Mapping Updated Successfully !');
                    redirect('rate/mapping', 'refresh');
                }
            } elseif($this->input->post()) {
                $this->session->set_flashdata('danger', stripErrors(validation_errors()));
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                $data = array('page' => 'rate/mapping_edit');
                $data['rates'] = $this->rate->getEarningRateIds();
                $data['types'] = $this->getDataTypes();
                $data['mapping'] = $this->mapping->getMapping($id);
                $this->load->view('template_admin', $data);
            }
        } else {
            redirect('login/notfound');
        }      
    }

}
