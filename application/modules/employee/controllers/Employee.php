<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use XeroPHP\Application\PrivateApplication;

class Employee extends Base_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('EmployeeModel', 'employee');
    }

    public function index() {
        $data['page'] = 'employee/index';

        $data['employees'] = $this->employee->getEmployees();

        $this->load->view('template_admin', $data);
    }
}
