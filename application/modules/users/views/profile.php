<!-- Breadcrumb -->
<div class="page-header">
  <div class="page-block">
    <div class="row align-items-center">
      <div class="col-sm-8">
        <div class="page-header-title">
          <h4 class="m-b-10"><?php echo fetchLine( 'Profile' ); ?></h4>
        </div>
        <ul class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="dashboard">
              <i class="feather icon-home"></i>
            </a>
          </li>
          <li class="breadcrumb-item">
            <a href="users/profile"><?php echo fetchLine( 'Profile' ); ?></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="pcoded-inner-content">
  <div class="main-body">
    <div class="page-wrapper">
      <!-- Page-body start -->
      <div class="page-body">

        <!--profile cover end-->
        <div class="row">
          <div class="col-lg-12">
            <!-- tab header end -->
            <!-- tab content start -->
            <div class="tab-content">
              <!-- tab panel personal start -->
              <div class="tab-pane active" id="personal" role="tabpanel">
                <!-- personal card start -->
                <div class="card">
                  <div class="card-header mb-2">
                    <h5 class="card-header-text"><?php echo fetchLine( 'About Me' ); ?></h5>
                  </div>
                  <div class="card-block">
                    <!-- end of view-info -->
                    <div class="edit-info">
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="general-info form-material">
                            <form action="<?= base_url('users/updateProfile/'. $user->id); ?>" method="POST" enctype="multipart/form-data">
                              <div class="row">
                                <div class="col-sm-4">
                                  <div class="form-group form-primary">
                                    <input type="text" name="name" class="form-control" required="" value="<?= $user->name; ?>">
                                    <span class="form-bar"></span>
                                    <label class="float-label"><?php echo fetchLine( 'Name' ); ?></label>
                                  </div>
                                </div>

                                <div class="col-sm-4">
                                  <div class="form-group form-primary">
                                    <input type="hidden" name="edit_username" value="<?= $user->username; ?>">
                                    <input type="text" name="username" class="form-control" required="" value="<?= $user->username; ?>">
                                    <span class="form-bar"></span>
                                    <label class="float-label"><?php echo fetchLine( 'Username' ); ?></label>
                                  </div>
                                </div>

                                <div class="col-sm-4">
                                  <div class="form-group form-primary">
                                    <input type="hidden" name="edit_email" value="<?= $user->email; ?>">
                                    <input type="text" name="email" class="form-control" required="" value="<?= $user->email; ?>">
                                    <span class="form-bar"></span>
                                    <label class="float-label"><?php echo fetchLine( 'Email' ); ?></label>
                                  </div>
                                </div>

                                <div class="col-sm-4">
                                  <div class="form-group form-primary">
                                    <input type="file" name="avatar" class="form-control" >
                                    <input type="hidden" name="edit_avatar" value="<?= $user->avatar; ?>" >
                                    <span class="form-bar"></span>
                                    <label class="float-label"><?php echo fetchLine( 'New Avatar' ); ?></label>
                                  </div>
                                </div>

                                <div class="col-sm-4">
                                  <div class="form-group form-primary">
                                    <div class="row">
                                      <div class="col-sm-4 ">
                                        <label ><?php echo fetchLine( 'Avatar' ); ?></label>
                                      </div>
                                    </div>
                                  </div>
                                  <?php
                                  if(file_exists('uploads/users/'.$user->avatar ) && $user->avatar != ''){
                                    ?>
                                    <img src="<?= base_url().USERPHOTOPATH.$user->avatar; ?>" class="img-responsve" width="100px">
                                    <?php
                                  }?>
                                </div>
                              </div>
                              <!-- end of table col-lg-6 -->
                              <div class="col-lg-6">

                              </div>
                              <!-- end of table col-lg-6 -->
                            </div>
                            <!-- end of row -->
                            <div class="row">
                              <div class="col-sm-12 text-right">
                                <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="icofont icofont-save"></i><?php echo fetchLine( 'Save' ); ?></button>

                              </div>
                            </div>


                          </div>
                          <!-- end of edit info -->
                        </div>
                        <!-- end of col-lg-12 -->
                      </div>
                      <!-- end of row -->
                    </form>
                  </div>
                  <!-- end of edit-info -->
                </div>
                <!-- end of card-block -->
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header">
                      <h5 class="card-header-text"><?php echo fetchLine( 'Change Password' ); ?></h5>

                    </div>
                    <div class="card-block user-desc">

                      <form action="<?= site_url('users/updatePassword'); ?>" method="POST" class="form-material">
                        <div class="row">
                          <div class="col-sm-4">

                            <div class="form-group">
                              <input type="password" name="password" class="form-control" required="">
                              <span class="form-bar"></span>
                              <label class="float-label"><?php echo fetchLine( 'Password' ); ?></label>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <input type="password" name="confirm_password" class="form-control" required="">
                              <span class="form-bar"></span>
                              <label class="float-label"><?php echo fetchLine( 'Confirm Password' ); ?></label>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-12 text-right">
                            <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="icofont icofont-save"></i><?php echo fetchLine( 'Save' ); ?></button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <!-- personal card end-->
            </div>
            <!-- tab pane personal end -->


          </div>
        </div>
        <!-- Page-body end -->
      </div>
    </div>
  </div>
</div>
<!-- Main body end -->

</div>
</div>
