<!-- Breadcrumb -->
<div class="page-header">
  <div class="page-block">
    <div class="row align-items-center">
      <div class="col-sm-8">
        <div class="page-header-title">
          <h4 class="m-b-10"><?php echo fetchLine( 'Users' ); ?></h4>
        </div>
        <ul class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="dashboard">
              <i class="feather icon-home"></i>
            </a>
          </li>
          <li class="breadcrumb-item">
            <a href="users"><?php echo fetchLine( 'Users' ); ?></a>
          </li>
          <li class="breadcrumb-item">
            <a href="users/editUser/<?=$user->id ?>"><?php echo fetchLine( 'Edit User' ); ?></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="pcoded-inner-content">
  <!-- Main-body start -->
  <div class="main-body">
    <div class="page-wrapper">
      <!-- Page-body start -->
      <div class="page-body">
        <div class="row">
          <div class="col-sm-12">
            <!-- Basic Form Inputs card start -->
            <div class="card">
              <div class="card-header border-0">
                <h5 class="pb-4"><i class="fa fa-user"></i> <?php echo fetchLine( 'Edit User' ); ?></h5>
                <div class="form-group form-primary d-inline-block float-right">
                  <label class="font-weight-semi mr-2"><?php echo fetchLine( 'Avatar' ); ?></label>
                  <?php if(file_exists(USERPHOTOPATH.$user->avatar) && $user->avatar != '') { ?>
                    <img src="<?= base_url().USERPHOTOPATH.$user->avatar; ?>" class="img-fluid rounded-circle" width="60px">
                  <?php } ?>
                  <input type="hidden" name="edit_avatar" value="<?= $user->avatar ?>">
                </div>
              </div>
              <div class="card-block">
                <form action="<?= site_url('users/editUser/'.$user->id); ?>" method="POST" enctype="multipart/form-data" class="form-material">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group form-primary">

                        <input type="text" class="form-control" name="name" value="<?= $user->name ?>" required="">
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Display Name' ); ?></label>
                      </div>
                    </div>

                    <div class="col-sm-4">
                      <div class="form-group form-primary">

                        <input type="text" class="form-control" name="email" value="<?= $user->email ?>" required="">
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Email' ); ?></label>
                        <input type="hidden" name="edit_email" value="<?= $user->email ?>">
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group form-primary input-wrapper">
                        <select name="role_id" class="form-control">
                          <?php foreach($roles as $role) { ?>
                            <option value="<?= $role->id ?>" <?php if($role->id == $user->role_id) echo 'selected'; ?> ><?= $role->name ?></option>
                          <?php } ?>

                        </select>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group form-primary">
                        <input type="text" class="form-control" name="username" value="<?= @$user->username ?>" required="">
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Username' ); ?></label>
                        <input type="hidden" name="edit_username" value="<?= $user->username ?>">

                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group form-primary">
                        <input type="password" class="form-control" name="password" value="" >
                        <label class="label label-danger pass_label"><i class="fa fa-info" data-toggle="tooltip" data-placement="top" data-original-title="Insert password to update"></i></label>
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Password' ); ?></label>

                      </div>
                    </div>

                    <div class="col-sm-4">
                      <div class="form-group form-primary">
                        <!-- <input type="file" class="form-control" name="avatar" > -->
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="customFile" name="avatar">
                          <label class="custom-file-label" for="customFile">Choose new avatar</label>
                        </div>
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'New Avatar' ); ?></label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <!-- <div class="col-sm-4">
                    <div class="form-group form-primary">
                    <div class="row">
                    <div class="col-sm-12">
                    <label ><?php echo fetchLine( 'Avatar' ); ?></label>
                  </div>
                </div>

                <?php if(file_exists(USERPHOTOPATH.$user->avatar) && $user->avatar != '') { ?>
                <img src="<?= base_url().USERPHOTOPATH.$user->avatar; ?>" class="img-fluid rounded-circle" width="100px">
              <?php } ?>
              <input type="hidden" name="edit_avatar" value="<?= $user->avatar ?>">

            </div>
          </div> -->

          <div class="col-sm-12 text-right">
            <hr class="my-3">
            <button type="submit" class="btn btn-green"><i class="icofont icofont-save"></i> <?php echo fetchLine( 'Save' ); ?></button>
          </div>

        </div>
      </form>
      <!-- Basic Form Inputs card end -->

    </div>
    <!-- Main-body end -->

  </div>
</div>
</div>

</div>
<!-- Page-body end -->
</div>
</div>
<!-- Main-body end -->

</div>

<style type="text/css">
.pass_label{
  position: absolute;
  bottom: 30px;
  right: 0px;
}

</style>
