<!-- Breadcrumb -->
<?php
$crumbs = [
    [
        'page_title' => fetchLine('Users'),
        'url' => base_url('users')
    ],
    [
        'page_title' => fetchLine('Permissions'),
        'url' => base_url('users/permissions')
    ]
];

$this->load->view('inc/breadcrumb', array('crumbs' => $crumbs));
?>

<div class="pcoded-inner-content">
  <!-- Main-body start -->
  <div class="main-body">
    <div class="page-wrapper">
      <!-- Page-body start -->
      <div class="page-body">
        <div class="row">
          <div class="col-sm-12">
            <!-- Zero config.table start -->
            <div class="card">
              <div class="card-header border-0">
                  <h5><i class="fa fa-shield-alt"></i> <?php echo fetchLine( 'List' ); ?></h5>
                <span class="d-inline-block pull-right">
                  <?php if ( $this->acl->has_permission( 'users-permissions-addPermission' ) ) { ?>
                    <a class="btn btn-info" href="<?= site_url( 'users/permissions/addPermission' ); ?>"><i
                      class="icofont icofont-plus"></i> <?php echo fetchLine( 'Add' ); ?></a>
                    <?php } ?>
                  </span>
                </div>
                <div class="card-block">
                  <div class="dt-responsive table-responsive">
                    <table id="simpletable" class="table table-striped table-bordered nowrap">
                      <thead>
                        <tr>
                          <th><?php echo fetchLine( 'Key' ); ?></th>
                          <th><?php echo fetchLine( 'Name' ); ?></th>
                          <th><?php echo fetchLine( 'Description' ); ?></th>
                          <th><?php echo fetchLine( 'Action' ); ?></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if ( $permissions ) {
                          foreach ( $permissions as $permission ) {
                            ?>
                            <tr>
                              <td><?= $permission->key; ?></td>
                              <td><?= $permission->name; ?></td>
                              <td><?= $permission->description; ?></td>
                              <td>
                                <?php if ( $this->acl->has_permission( 'users-permissions-editPermission' ) ) { ?>
                                  <a href="<?= site_url( 'users/permissions/editPermission/' . $permission->id ) ?>"
                                    title="Edit Permission"
                                    class="btn btn-mini btn-info"><i
                                    class="icofont icofont-ui-edit p-r-3"></i><?php echo fetchLine( 'Edit' ); ?></a>
                                  <?php } ?>
                                  <?php if ( $this->acl->has_permission( 'users-permissions-delete' ) ) { ?>
                                    <a href="#"
                                    title="Delete Permission" class="btn btn-danger btn-mini"
                                    onclick="return confirm('<?php echo fetchLine( 'Are you sure you want to delete?' ); ?>');"><i
                                    class="icofont icofont-ui-delete p-r-3"></i><?php echo fetchLine( 'Del' ); ?>
                                  </a>
                                <?php } ?>
                              </td>
                            </tr>

                          <?php }
                        } ?>

                      </tbody>
                      <tfoot>
                        <tr>
                          <th><?php echo fetchLine( 'Key' ); ?></th>
                          <th><?php echo fetchLine( 'Name' ); ?></th>
                          <th><?php echo fetchLine( 'Description' ); ?></th>
                          <th><?php echo fetchLine( 'Action' ); ?></th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
              <!-- Zero config.table end -->
            </div>
          </div>
        </div>
        <!-- Page-body end -->
      </div>
    </div>
    <!-- Main-body end -->
  </div>
