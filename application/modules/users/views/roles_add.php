<!-- Breadcrumb -->
<div class="page-header">
  <div class="page-block">
    <div class="row align-items-center">
      <div class="col-sm-8">
        <div class="page-header-title">
          <h4 class="m-b-10"><?php echo fetchLine( 'Roles' ); ?></h4>
        </div>
        <ul class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="dashboard">
              <i class="feather icon-home"></i>
            </a>
          </li>
          <li class="breadcrumb-item">
            <a href="users/roles"><?php echo fetchLine( 'Roles' ); ?></a>
          </li>
          <li class="breadcrumb-item">
            <a href="users/roles/addRole"><?php echo fetchLine( 'Add Role' ); ?></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="pcoded-inner-content">
  <!-- Main-body start -->
  <div class="main-body">
    <div class="page-wrapper">
      <!-- Page-body start -->
      <div class="page-body">
        <div class="row">
          <div class="col-sm-12">
            <!-- Basic Form Inputs card start -->
            <div class="card">
              <div class="card-header border-0">
                <h5 class="pb-3"><i class="fa fa-user"></i> <?php echo fetchLine( 'Add Role' ); ?></h5>
              </div>
              <div class="card-block">
                <form action="<?= site_url('users/roles/addRole'); ?>" method="POST" class="form-material">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group form-primary">
                        <input type="text" class="form-control" name="name" value="<?= set_value('name'); ?>" required="">
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Name' ); ?></label>
                        <i class="help-block small text-muted">* <?php echo fetchLine( 'Role name' ); ?></i>
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group form-primary">
                        <input type="text" class="form-control" name="description" value="<?= set_value('description'); ?>" required="">
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Description' ); ?></label>
                        <i class="help-block small text-muted">* <?php echo fetchLine( 'Name to identify the permission type' ); ?> </i>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group form-primary role-permissions">
                        <label class="secondary-title"><?php echo fetchLine( 'Permission' ); ?></label>
                        <select multiple="multiple" class="form-control multiple-select" id="my-select" name="permissions[]" style="height: 150px !important;" required="">
                          <?php
                          if (!empty($permissions)) {

                           foreach ($permissions as $k => $module) { ?>
                              <optgroup style="padding-left: 15px;" label="<?= $k ?>">
                                <?php foreach ($module as $controller) { ?>
                                  <option value="<?= $controller->id ?>"><?= $controller->key ?></option>
                                <?php } ?>
                              </optgroup>
                            <?php }

                          }
                          ?>

                        </select>

                      </div>
                    </div>

                    <div class="col-sm-12 text-right">
                      <hr class="my-3">
                      <button type="submit" class="btn btn-green"><i class="icofont icofont-save"></i> <?php echo fetchLine( 'Save' ); ?></button>
                    </div>
                  </div>
                </form>
                <!-- Basic Form Inputs card end -->

              </div>

              <!-- Main-body end -->

            </div>
          </div>
        </div>

      </div>
      <!-- Page-body end -->
    </div>
  </div>
  <!-- Main-body end -->

</div>
