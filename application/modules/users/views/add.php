<!-- Breadcrumb -->
<div class="page-header">
  <div class="page-block">
    <div class="row align-items-center">
      <div class="col-sm-8">
        <div class="page-header-title">
          <h4 class="m-b-10"><?php echo fetchLine( 'Users' ); ?></h4>
        </div>
        <ul class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="dashboard">
              <i class="feather icon-home"></i>
            </a>
          </li>
          <li class="breadcrumb-item">
            <a href="users"><?php echo fetchLine( 'Users' ); ?></a>
          </li>
          <li class="breadcrumb-item">
            <a href="users/addUser"><?php echo fetchLine( 'Add User' ); ?></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="pcoded-inner-content">
  <!-- Main-body start -->
  <div class="main-body">
    <div class="page-wrapper">
      <!-- Page-body start -->
      <div class="page-body">
        <div class="row">
          <div class="col-sm-12">
            <!-- Basic Form Inputs card start -->
            <div class="card">
              <div class="card-header border-0">
                <h5 class="pb-3"><i class="fa fa-user"></i> <?php echo fetchLine( 'Add User' ); ?></h5>
              </div>
              <div class="card-block">
                <form action="<?= base_url( 'users/addUser' ); ?>" method="POST"
                  enctype="multipart/form-data" class="form-material">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group form-primary">
                        <input type="text" class="form-control" name="name"
                        value="<?= set_value( 'name' ); ?>" required="">
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Display Name' ); ?></label>
                      </div>
                    </div>

                    <div class="col-sm-4">
                      <div class="form-group form-primary">
                        <input type="text" class="form-control" name="email"
                        value="<?= set_value( 'email' ); ?>" required="">
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Email' ); ?></label>

                      </div>
                    </div>

                    <div class="col-sm-4">
                      <div class="form-group form-primary select input-wrapper">
                        <select name="role_id" class="form-control">
                          <?php foreach ( $roles as $role ) { ?>
                            <option value="<?= $role->id ?>"><?= $role->name ?></option>
                          <?php } ?>

                        </select>
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Role' ); ?></label>

                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group form-primary">
                        <input type="text" class="form-control" name="username"
                        value="<?= set_value( 'username' ); ?>" required="">
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Username' ); ?></label>

                      </div>
                    </div>


                    <div class="col-sm-4">
                      <div class="form-group form-primary">

                        <input type="password" class="form-control" name="password" required="">
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Password' ); ?></label>

                      </div>
                    </div>

                    <div class="col-sm-4">
                      <div class="form-group form-primary">

                        <input type="password" class="form-control" name="confirm_password"
                        required="">
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Confirm Password' ); ?></label>

                      </div>
                    </div>

                    <div class="col-sm-4">
                      <div class="form-group form-primary">
                        <label><?php echo fetchLine( 'Avatar' ); ?></label>
                        <!-- <input type="file" class="form-control" name="avatar"> -->
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="customFile" name="avatar">
                          <label class="custom-file-label" for="customFile">Choose avatar</label>
                        </div>
                        <span class="form-bar"></span>
                      </div>
                    </div>
                    <div class="col-12">
                      <hr class="my-3">
                    </div>
                    <div class="col-sm-12 text-right">
                      <button type="submit" class="btn btn-green"><i
                        class="icofont icofont-save"></i> <?php echo fetchLine( 'Save' ); ?>
                      </button>
                    </div>
                  </div>
                </form>
                <!-- Basic Form Inputs card end -->

              </div>
            </div>
            <!-- Main-body end -->

          </div>
        </div>
      </div>

    </div>
    <!-- Page-body end -->
  </div>
</div>
<!-- Main-body end -->
