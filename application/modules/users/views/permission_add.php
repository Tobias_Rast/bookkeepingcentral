<!-- Breadcrumb -->
<div class="page-header">
  <div class="page-block">
    <div class="row align-items-center">
      <div class="col-sm-8">
        <div class="page-header-title border-0">
          <h4 class="m-b-10"><?php echo fetchLine( 'Permissions' ); ?></h4>
        </div>
        <ul class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="dashboard">
              <i class="feather icon-home"></i>
            </a>
          </li>
          <li class="breadcrumb-item">
            <a href="users/permissions"><?php echo fetchLine( 'Permissions' ); ?></a>
          </li>
          <li class="breadcrumb-item">
            <a href="users/permissions/addPermission"><?php echo fetchLine( 'Add Permission' ); ?></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="pcoded-inner-content">
  <!-- Main-body start -->
  <div class="main-body">
    <div class="page-wrapper">
      <!-- Page-body start -->
      <div class="page-body">
        <div class="row">
          <div class="col-sm-12">
            <!-- Basic Form Inputs card start -->
            <div class="card">
              <div class="card-header border-0">
                <h5 class="mb-3"><i class="fa fa-user"></i> <?php echo fetchLine( 'Add Permission' ); ?></h5>
              </div>
              <div class="card-block">
                <form action="<?= site_url('users/permissions/addPermission'); ?>" method="POST" class="form-material">
                  <div class="row">
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group form-primary select input-wrapper">
                        <select class="form-control" name="module" id="module" >
                          <option value=""><?php echo fetchLine( 'Select module' ); ?></option>
                          <?php foreach (getModules() as $module) { ?>
                            <option value="<?= $module ?>"><?= ucfirst($module) ?></option>
                          <?php } ?>
                        </select>
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Module' ); ?></label>
                        <i class="help-block small text-muted"><?php echo fetchLine( 'Module Name' ); ?></i>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group form-primary select input-wrapper">
                        <select class="form-control" name="controller" id="controller" >
                          <option value=""><?php echo fetchLine( 'Select controller' ); ?></option>
                        </select>
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Controller' ); ?></label>
                        <i class="help-block small text-muted"><?php echo fetchLine( 'Controller' ); ?></i>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group form-primary">
                        <select class="form-control my-select" name="methods[]" id="method" multiple="multiple" >
                          <option value=""><?php echo fetchLine( 'Select method' ); ?></option>
                        </select>
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Method' ); ?></label>
                        <i class="help-block small text-muted"><?php echo fetchLine( 'Method' ); ?> </i>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group form-primary">

                        <input type="text" class="form-control" name="name" value="<?= set_value('name'); ?>" required="">
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Name' ); ?></label>
                        <i class="help-block small text-muted"><?php echo fetchLine( 'Name to identify the permission type' ); ?> </i>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group form-primary">

                        <input type="text" class="form-control" name="description" value="<?= set_value('description'); ?>" required="">
                        <span class="form-bar"></span>
                        <label class="float-label"><?php echo fetchLine( 'Description' ); ?></label>
                        <i class="help-block small text-muted"><?php echo fetchLine( 'Description of the permission' ); ?> </i>
                      </div>
                    </div>
                    <div class="col-sm-12 text-right">
                      <hr class="my-3">
                      <button type="submit" class="btn btn-green"><i class="icofont icofont-save"></i><?php echo fetchLine( 'Save' ); ?></button>
                    </div>
                  </div>
                </form>
                <!-- Basic Form Inputs card end -->
              </div>
              <!-- Main-body end -->
            </div>
          </div>
        </div>

      </div>
      <!-- Page-body end -->
    </div>
  </div>
  <!-- Main-body end -->
</div>
<script>
$(function () {
  $(document).on('change', '#module', function () {
    $.post("<?= site_url('users/permissions/getClassesByModule') ?>", {module: $(this).val()}, function (data) {
      $('#controller').children('option:not(:first)').remove();
      $('#method').children('option:not(:first)').remove();

      $('#controller').prop('disabled', false).prop('optional', false).removeClass('optional');
      $.each(data, function (key, value) {
        $('#controller').append($("<option></option>").attr("value", value).text(value));
      });

    }, "json");
  });
  $(document).on('change', '#controller', function () {
    $module = $('#module option:selected').val();
    $.post("<?= site_url('users/permissions/getMethodsByController') ?>", {module: $module, controller: $(this).val()}, function (data) {
      $('#method').children('option:not(:first)').remove();
      $.each(data, function (key, value) {
        $('#method').append($("<option></option>").attr("value", value).text(value));
      });

    }, "json");
  });

})
</script>
