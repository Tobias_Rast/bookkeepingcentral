<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Base_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('users_model', 'model');
    }

    public function index() {
        $data = array('page' => 'users/index');
        $data['users'] = $this->model->getAllUsers(array('role_id !=' => 6));
        $this->load->view('template_admin', $data);
    }

    public function addUser(){
        
        if ($this->_formValidationAdd()) {

            $time = time();
            if (isset($_FILES['avatar']['name']) && $_FILES['avatar']['name'] != '') {
                $filename = $time . $_FILES['avatar']['name'];
                move_uploaded_file($_FILES['avatar']['tmp_name'], USERPHOTOPATH . $filename);
            } else {
                $filename = '';
            }
            if ($this->model->addUser($this->input->post(), $filename)) {
                $this->session->set_flashdata('success', 'User added successfully !');
                redirect('users', 'refresh');
            }
        } elseif ($this->input->post()) {
           
            $this->session->set_flashdata('danger', stripErrors(validation_errors()));
            redirect($_SERVER['HTTP_REFERER']);          
        } else {
            $data = array('page' => 'users/add');
            $data['roles'] = $this->model->getRoles();
            $this->load->view('template_admin', $data);
        }
                
    }

   
    public function editUser( $id ){
        if ($this->_formValidationEdit()) {
            $time = time();
            if (isset($_FILES['avatar']['name']) && $_FILES['avatar']['name'] != '') {

                if ($this->input->post('edit_avatar') != '') {
                    $existingPhoto = $this->input->post('edit_avatar');
                    $filename = USERPHOTOPATH . $existingPhoto;
                    if (file_exists($filename)) {
                        unlink($filename);
                    }
                }

                $filename = $time . $_FILES['avatar']['name'];
                move_uploaded_file($_FILES['avatar']['tmp_name'], USERPHOTOPATH . $filename);
            }else{
                $filename = $this->input->post('edit_avatar');
            } 

            if ($this->model->editUser($id, $this->input->post(), $filename)) {
                $this->session->set_flashdata('success', 'User Updated successfully !');
                redirect('users', 'refresh');
            }
        } elseif ($this->input->post()) {
            $this->session->set_flashdata('danger', stripError(svalidation_errors()));
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $data = array('page' => 'users/edit');
            $data['user'] = $this->model->getUserById( $id );
            $data['roles'] = $this->model->getRoles();
            $this->load->view('template_admin', $data);
        }
                
    }

    public function delete($id, $filename = '') {
        if ($filename != '') {
            if (file_exists(USERPHOTOPATH . $filename)) {
                unlink(USERPHOTOPATH . $filename);
            }
        }

        if ($this->model->deleteUser($id)) {
            $this->session->set_flashdata('success', 'User deleted successfully !');
        } else {
            $this->session->set_flashdata('danger', 'Problem Deleting!');
        }
        redirect('users', 'refresh');
    }


    private function _formValidationAdd() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[users.username]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|is_unique[users.email]|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');

        $this->form_validation->set_message('required', '%s required !');
        $this->form_validation->set_message('is_unique', '%s already exists !');
        $this->form_validation->set_message('valid_email', 'Invalid %s !');
        $this->form_validation->set_message('matches', '%s does not match !');

        return $this->form_validation->run();
    }

    private function _formValidationEdit() {
        $this->load->library('form_validation');

        if ($this->input->post('edit_email') != $this->input->post('email')) {
            $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|is_unique[users.email]');
        }

        if ($this->input->post('edit_username') != $this->input->post('username')) {
            $this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[users.username]');
        }
        $this->form_validation->set_rules('name', 'Name', 'trim|required');

        $this->form_validation->set_message('required', '%s required !');
        $this->form_validation->set_message('is_unique', '%s already exists !');
        $this->form_validation->set_message('valid_email', 'Invalid %s !');
        
        return $this->form_validation->run();
    }

    public function profile(){
        $id = $this->session->userdata('id');
        $data = array('page' => 'users/profile');
        $data['user'] = $this->model->getUserById( $id );
        $this->load->view('template_admin', $data);
    }

    public function updateProfile( $id ) {
        
        if ($this->_formValidationEdit()) {
            $time = time();
            if (isset($_FILES['avatar']['name']) && $_FILES['avatar']['name'] != '') {

                if ($this->input->post('edit_avatar') != '') {
                    $existingPhoto = $this->input->post('edit_avatar');
                    $filename = USERPHOTOPATH . $existingPhoto;
                    if (file_exists($filename)) {
                        unlink($filename);
                    }
                }

                $filename = $time . $_FILES['avatar']['name'];
                move_uploaded_file($_FILES['avatar']['tmp_name'], USERPHOTOPATH . $filename);
            }else{
                $filename = $this->input->post('edit_avatar');
            } 

            if ($this->model->updateUserProfile($id, $this->input->post(), $filename)) {
                $this->session->set_flashdata('success', 'User Updated successfully !');
                redirect('users/profile', 'refresh');
            }
        } else {
            $this->session->set_flashdata('danger', stripErrors(validation_errors()));
            redirect($_SERVER['HTTP_REFERER']);

        }

        
       
    }

    public function updatePassword(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');

        $this->form_validation->set_message('required', '%s required !');
        $this->form_validation->set_message('matches', '%s does not match !');
        $id  = $this->session->userdata('id');

        if($this->form_validation->run()){
            $password = $this->input->post('password');
            
            $this->model->updatePassword( $password, $id );
            $this->session->set_flashdata('success', 'Password Updated successfully !');
            redirect('users/profile', 'refresh');
        }else {
            $this->session->set_flashdata('danger', stripErrors(validation_errors()));
            redirect($_SERVER['HTTP_REFERER']);

        }

    }

}
