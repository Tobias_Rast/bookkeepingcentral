<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Roles extends Base_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Roles_model', 'roles', true);
        $this->user_id = $this->session->userdata('user_id');
    }

    public function index()
    {
        $data = array('page' => 'users/roles_list');
        $data['roles'] = $this->roles->getRoles();
        
        $this->load->view('template_admin', $data);
        
    }

    public function addRole()
    {    
        $this->form_validation->set_rules('name', 'Name', 'required');

        $this->form_validation->set_message('required', '%s required !');
        if ($this->form_validation->run() == true) {
            if ($this->roles->addRole($this->input->post())) {
                $this->session->set_flashdata('success', 'Role Added Successfully !');
                redirect('users/roles/addRole', 'refresh');
            }
        } elseif ($this->input->post()) {

            $this->session->set_flashdata('danger', stripErrors(validation_errors()));
            redirect($_SERVER['HTTP_REFERER']);
        }else {
            $data = array('page' => 'users/roles_add');

            $permissons = $this->roles->getPermissions();

            foreach ($permissons as $key => $item) {
                $exploded_key = explode('-',$item->key);
                $grouped_permissons[$exploded_key[0].'-'.$exploded_key[1]][$key] = $item;
            }
            ksort($grouped_permissons, SORT_NUMERIC);
            
            $data['permissions'] = $grouped_permissons;
            $this->load->view('template_admin', $data);
        }
        
    }

    public function editRole($role_id)
    {
        if ($this->roles->getRole($role_id)) {
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            
            $this->form_validation->set_message('required', '%s required !');
            
            if ($this->form_validation->run() == true) {
                if ($this->roles->updateRole($this->input->post(), $role_id)) {
                    $this->session->set_flashdata('success', 'Role Updated Successfully !');

                    redirect('users/roles', 'refresh');
                }
            } elseif($this->input->post()) {
                $this->session->set_flashdata('danger', stripErrors(validation_errors()));
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                $data = array('page' => 'users/roles_edit');

                $permissons = $this->roles->getPermissions();
                foreach ($permissons as $key => $item) {
                    $exploded_key = explode('-',$item->key);
                    $grouped_permissons[$exploded_key[0].'-'.$exploded_key[1]][$key] = $item;
                }

                if (!empty($grouped_permissons)) {
                    ksort($grouped_permissons, SORT_NUMERIC);
                    $data['permissions'] = $grouped_permissons;
                }
                
                $data['role'] = $this->roles->getRole($role_id);

                $this->load->view('template_admin', $data);
            }
        } else {
            redirect('login/notfound');
        }      

    }

    public function delete($role_id)
    {
        if ($this->roles->getRole($role_id)) {
            if ($this->roles->deleteRole($role_id)) {
                $this->session->set_flashdata('success', fetchLine('Role Deleted Successfully !'));
                redirect('users/roles', 'refresh');
            }
        } else {
            redirect('login/notfound');
        }

    }


}
