<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Permissions extends Base_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Roles_model', 'roles', true);
        $this->user_id = $this->session->userdata('user_id');
    }

    public function index() {
        $data = array('page' => 'users/permission_list');
        $data['permissions'] = $this->roles->getPermissions();
        $this->load->view('template_admin', $data);
    }

    public function addPermission() {
        
        $this->form_validation->set_rules('module', 'Module', 'required');
        $this->form_validation->set_rules('controller', 'Controller', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');

        $this->form_validation->set_message('required', '%s required !');

        if ($this->form_validation->run() == true) {
            $insert_data = [];
            foreach ($this->input->post('methods') as $method) {
                $key = $this->input->post('module') . '-' . $this->input->post('controller') . '-' . $method;
                if(!$this->roles->isKeyExists( $key )){
                    $insert_data[] = [
                    'name' => $this->input->post('name'),
                    'key' => strtolower($key),
                    'description' => $this->input->post('description')];
                }
                
            }

            if(!empty($insert_data)){
                if ($this->roles->addPermission($insert_data)) {
                    $this->session->set_flashdata('success', 'Permission Stored successfully !');
                    redirect('users/permissions/addPermission', 'refresh');
                }
            }else{
                $this->session->set_flashdata('danger', 'Permission already exists !');
                redirect($_SERVER['HTTP_REFERER']);
            }
            
        } else if($this->input->post()) {
            $this->session->set_flashdata('danger', stripErrors(validation_errors()));
            redirect($_SERVER['HTTP_REFERER']);
        } else {

            $data = array('page' => 'users/permission_add');
            $this->load->view('template_admin', $data);
        }
    }

    public function getClassesByModule() {
        $module = $this->input->post('module');
        echo json_encode(getClasses($module));
        exit;
    }

    public function getMethodsByController() {
        $module = $this->input->post('module');
        $controller = $this->input->post('controller');
        echo json_encode(getMethods($module, $controller));
        exit;
    }

    public function editPermission($permission_id = false) {
        if ($this->roles->getPermission($permission_id)) {
            
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('module', 'Module', 'required');
            $this->form_validation->set_rules('controller', 'Controller', 'required');
            $this->form_validation->set_rules('method', 'Method', 'required');
            
            $this->form_validation->set_message('required', '%s required !');

            if ($this->form_validation->run() == true) {
                $key = $this->input->post('module') . '-' . $this->input->post('controller') . '-' . $this->input->post('method');

                $insert_data = array('name' => $this->input->post('name'),
                    'key' => strtolower($key),
                    'description' => $this->input->post('description'));

                if(!$this->roles->isKeyExists( $key, $permission_id )){
                    if ($this->roles->updatePermission($insert_data, $permission_id)) {
                        $this->session->set_flashdata('success', 'Permission Updated successfully !');
                        redirect('users/permissions', 'refresh');
                    } 
                }else{
                    $this->session->set_flashdata('danger', 'Permission already exists !');
                    redirect($_SERVER['HTTP_REFERER']);
                }
                
            } elseif($this->input->post()) {
                $this->session->set_flashdata('danger', stripErrors(validation_errors()));
                redirect($_SERVER['HTTP_REFERER']);
            }else{

                $data = array('page' => 'users/permission_edit');
                $data['permission'] = $this->roles->getPermission($permission_id);
                $this->load->view('template_admin', $data);
            }

        } else {
            redirect('login/notfound');
        }
    }

    public function delete($permission_id) {
        if ($this->roles->getPermission($permission_id)) {
            if ($this->roles->deletePermission($permission_id)) {
                $this->session->set_flashdata('success', 'Permission Deleted successfully !');
            } else {
                $this->session->set_flashdata('danger', 'Error occured while removing permission !');
            }
            redirect('users/permissions', 'refresh');
        } else {
            redirect('login/notfound');
        }
    }

}
