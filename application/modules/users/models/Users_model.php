<?php

if ( ! defined( 'BASEPATH' ) ) {

	exit( 'No direct script access allowed' );
}

class Users_model extends MY_Model {

	private $table_user = "users";
	private $user_roles = array( 1, 2 ); // 1 = Superadmin, 2 = Admin

	public function __construct() {

		parent::__construct();
		$this->table = $this->table_user;
	}

	public function getUserById( $user_id ) {
		return $this->db->select( '*' )
		                ->from( $this->table_user )
		                ->where( 'id ', $user_id )
			// ->where_in('role_id',$this->user_roles)
			            ->get()->row();
	}

	public function updatePassword( $password, $user_id ) {
		$data = array( 'password' => hashPassword( $password ) );

		return $this->db->update( $this->table_user, $data, array( 'id' => $user_id ) );
	}

	public function updateUserProfile( $id, $data, $filename ) {
		$userData = array(
			'email'    => $data['email'],
			'name'     => $data['name'],
			'username' => $data['username'],
			'avatar'   => $filename,
		);

		if ( $return = $this->db->update( $this->table_user, $userData, array( 'id' => $id ) ) ) {
			if ( $filename ) {
				$sessdata['avatar'] = $filename;
			} else {
				$sessdata['avatar'] = '';
			}

			//change session data
			$sessdata = array(
				'user_id'   => $id,
				'name'      => $data['name'],
				'username'  => $data['username'],
				'email'     => $data['email'],
				'logged_in' => true,
			);
			$this->session->set_userdata( $sessdata );

			return $return;
		}

		return false;

	}

	public function getAllUsers( $filters = false ) {

		$this->db->select( "$this->table_user.*,role.name as role " )
		         ->from( $this->table_user )
		         ->join( 'roles as role', $this->table_user . '.role_id=role.id' );
		$this->db->where( "$this->table_user.id !=", 1 );
		$this->db->where( "status", 1 );
		if ( $filters ) {
			$this->db->where( $filters );
		}

		$result = $this->db->get()->result();

		return $result;
	}

	public function getRoles() {
		$this->db->where( 'id !=', 1 );

		return $this->db->select( '*' )->from( 'roles' )->get()->result();
	}

	public function addUser( $data, $filename ) {

		$userData = array(
			'email'    => $data['email'],
			'password' => hashPassword( $data['password'] ),
			'name'     => $data['name'],
			'username' => $data['username'],
			'status'   => 1,
			'avatar'   => $filename,
			'role_id'  => $data['role_id'],
			'token'    => md5( date( 'Y-m-d H:i:s' ) )
		);
		$this->db->insert( $this->table_user, $userData );

		return true;
	}

	public function editUser( $id, $data, $filename ) {

		$userData = array(
			'email'    => $data['email'],
			'name'     => $data['name'],
			'username' => $data['username'],
			//'status' => $data['status'],
			'avatar'   => $filename,
			'role_id'  => $data['role_id'],
		);
		if ( $data['password'] != '' ) {
			$userData['password'] = hashPassword( $data['password'] );
		}

		if ( $return = $this->db->update( $this->table_user, $userData, array( 'id' => $id ) ) ) {
			if ( $filename ) {
				$this->session->set_userdata( 'image', $filename );
			} else {
				$this->session->set_userdata( 'image', '' );
			}

			return $return;
		}

		return false;
	}

	public function deleteUser( $id ) {

		return $this->db->update( $this->table_user,
			array(
				'status'              => 0,
				'status_changed_date' => date( 'Y-m-d H:i:s' ),
			),
			array( 'id' => $id )
		);
	}

	public function isEmailExists( $email, $user_id ) {
		$this->db->select();
		$this->db->where( 'email', $email );
		$this->db->where( 'id !=', $user_id );
		$result = $this->db->get( $this->table_user )->row();
		if ( $result ) {
			return true;
		} else {
			return false;
		}
	}

	public function isUsernameExists( $username, $user_id ) {
		$this->db->select();
		$this->db->where( 'username', $username );
		$this->db->where( 'id !=', $user_id );
		$result = $this->db->get( $this->table_user )->row();
		if ( $result ) {
			return true;
		} else {
			return false;
		}
	}

}
