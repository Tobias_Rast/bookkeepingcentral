<!-- Breadcrumb -->
<?php
$crumbs = [
    [
        'page_title' => fetchLine('Employees'),
        'url' => base_url('employee')
    ],
    [
        'page_title' => fetchLine('List of Employee'),
        'url' => base_url('employee')
    ]
];
$this->load->view('inc/breadcrumb', array('crumbs' => $crumbs));

?>
<div class="pcoded-inner-content">  
    <!-- Main-body start -->
    <div class="main-body">
        <div class="page-wrapper">
            <!-- Page-body start -->
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Zero config.table start -->
                        <div class="card">
                          <div class="card-header border-0">
                            <h5><i class="fa fa-group"></i> <?php echo fetchLine('List of Employee'); ?></h5>
                          </div>
                          <div class="card-block">
                            <div class="dt-responsive table-responsive">
                              <table id="base-style" class="table table-striped table-bordered nowrap dataTable" role="grid" aria-describedby="base-style_info">
                                <thead>
                                  <tr>
                                    <th><?php echo fetchLine('First Name'); ?></th>
                                    <th><?php echo fetchLine('Last Name'); ?></th>
                                    <th><?php echo fetchLine('Employee ID'); ?></th>
                                    <th><?php echo fetchLine('Email'); ?></th>
                                    <th><?php echo fetchLine('Phone'); ?></th>
                                    <th><?php echo fetchLine('Status'); ?></th>
                                    <th><?php echo fetchLine('Action'); ?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php if($employees){
                                    foreach($employees as $employee) {
                                      ?>
                                      <tr>
                                        <td><?= $employee->FirstName; ?></td>
                                        <td><?= $employee->LastName; ?></td>
                                        <td><?= $employee->EmployeeID; ?></td>
                                        <td><?= $employee->Email; ?></td>
                                        <td><?= $employee->Phone; ?></td>
                                        <td><?= $employee->Status; ?></td>
                                        <td> &nbsp; </td>
                                      </tr>
                                    <?php }
                                  } ?>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        <!-- Zero config.table end -->

                    </div>
                </div>
            </div>
            <!-- Page-body end -->
        </div>
    </div>
    <!-- Main-body end -->
</div>