<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeeModel extends CI_Model {

    public function getEmployees(){
        $returns = $this->db->get('employees')->result();
        return $returns;
    }

    public function saveEmployee($data) {
        $employee = $this->employeeExist($data['EmployeeID']);
        if ($employee) {
            $this->db->update('employees', $data, array('Id' => $employee->Id));
        } else {
            $this->db->insert('employees', $data); 
        }
    }

    public function employeeExist($employeeId) {
        $this->db->select('Id');
        $this->db->where('EmployeeID', $employeeId);
        $this->db->limit(1);
        $query = $this->db->get('employees');
        $result = $query->result();
        if (!empty($result)) {
            return $result[0];
        }
        return FALSE;
    }

    public function getEmployeeID($email) {
        $email = trim($email);
        $this->db->select('Id, EmployeeID');
        $this->db->where('Email', $email);
        $this->db->limit(1);
        $query = $this->db->get('employees');
        $result = $query->row();
        return $result;
    }
    
}
