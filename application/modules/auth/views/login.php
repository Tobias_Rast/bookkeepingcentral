<!DOCTYPE html>
<html lang="en">

<head>
  <title><?= SITE_NAME ?> | Control Panel</title>
  <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 10]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Meta -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <meta name="author" content="Pegotec Pte. Ltd." />
  <base href="<?= site_url() ?>">
  <!-- Favicon icon -->
  <link rel="icon" href="files/assets/images/favicon.ico" type="image/x-icon">
  <!-- Google font-->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
  <!-- Required Fremwork -->
  <link rel="stylesheet" type="text/css" href="files/bower_components/bootstrap/css/bootstrap.min.css">
  <!-- waves.css -->
  <link rel="stylesheet" href="files/assets/pages/waves/css/waves.min.css" type="text/css" media="all"><!-- feather icon --> <link rel="stylesheet" type="text/css" href="files/assets/icon/feather/css/feather.css">
  <!-- themify-icons line icon -->
  <link rel="stylesheet" type="text/css" href="files/assets/icon/themify-icons/themify-icons.css">
  <!-- ico font -->
  <link rel="stylesheet" type="text/css" href="files/assets/icon/icofont/css/icofont.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" type="text/css" href="files/assets/icon/font-awesome/css/font-awesome.min.css">
  <!-- Style.css -->
  <link rel="stylesheet" type="text/css" href="files/assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="files/assets/css/main.css">
  <link rel="stylesheet" type="text/css" href="files/assets/css/pages.css">
</head>

<body themebg-pattern="theme0">
  <!-- Pre-loader start -->
  <div class="theme-loader">
    <div class="loader-track">
      <div class="preloader-wrapper">
        <div class="spinner-layer spinner-blue">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
        <div class="spinner-layer spinner-red">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>

        <div class="spinner-layer spinner-yellow">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>

        <div class="spinner-layer spinner-green">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Pre-loader end -->
  <div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <!-- Menu header end -->
    <section class="login-block m-0">
      <!-- Container-fluid starts -->

      <div class="container-fluid text-center">
        <!-- <img src="files/assets/images/logo.svg"> -->
        <div class="row">
          <div class="col-sm-12" style="padding:0;">
            <!-- Authentication card start -->
            <form class="md-float-material form-material" method="post" action="">
              <div class="text-center teal-gradient py-5">
<!--                <img src="files/assets/images/logo.png" class="logo" alt="logo.png">-->
                  <h1><?=SITE_NAME?></h1>
                <div class="auth-box card mt-5">
                  <div class="card-block">
                    <div class="row m-b-20">
                      <div class="col-md-12">
                        <h3 class="text-center">Sign In</h3>
                      </div>
                    </div>
                    <div class="form-group form-primary">
                      <input type="text" name="username" class="form-control" value="" autocomplete="off" >
                      <span class="form-bar"></span>
                      <label class="float-label">Email / Username</label>
                    </div>
                    <div class="form-group form-primary">
                      <input type="password" name="password" class="form-control" value="">
                      <span class="form-bar"></span>
                      <label class="float-label">Password</label>
                    </div>
                    <div class="row m-t-25 text-left">
                      <div class="col-12">
                        <div class="checkbox-fade fade-in-primary d-">
                          <label>
                            <input type="checkbox" value="">
                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                            <span class="text-inverse">Remember me</span>
                          </label>
                        </div>
                        <div class="forgot-phone text-right float-right">
                          <a href="auth/forgotPassword" class="text-right f-w-600"> Forgot Password?</a>
                        </div>
                      </div>
                    </div>
                    <div class="row m-t-30">
                      <div class="col-md-12">
                        <input type="submit" value="Sign in" class="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </form>
            <!-- Authentication card end -->
          </div>
          <!-- end of col-sm-12 -->
        </div>
        <!-- end of row -->
      </div>
      <!-- end of container-fluid -->

    </section>
  </div>


  <div class="footer">
    <p class="text-center m-b-0">Copyright &copy; <?= date('Y') ?> , All rights reserved.</p>
  </div>
  <!-- Warning Section Starts -->
  <!-- Older IE warning message -->
  <!--[if lt IE 10]>
  <div class="ie-warning">
  <h1>Warning!!</h1>
  <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
  <div class="iew-container">
  <ul class="iew-download">
  <li>
  <a href="http://www.google.com/chrome/">
  <img src="files/assets/images/browser/chrome.png" alt="Chrome">
  <div>Chrome</div>
</a>
</li>
<li>
<a href="https://www.mozilla.org/en-US/firefox/new/">
<img src="files/assets/images/browser/firefox.png" alt="Firefox">
<div>Firefox</div>
</a>
</li>
<li>
<a href="http://www.opera.com">
<img src="files/assets/images/browser/opera.png" alt="Opera">
<div>Opera</div>
</a>
</li>
<li>
<a href="https://www.apple.com/safari/">
<img src="files/assets/images/browser/safari.png" alt="Safari">
<div>Safari</div>
</a>
</li>
<li>
<a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
<img src="files/assets/images/browser/ie.png" alt="">
<div>IE (9 & above)</div>
</a>
</li>
</ul>
</div>
<p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jquery -->
<script type="text/javascript" src="files/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="files/bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="files/bower_components/bootstrap/js/bootstrap.min.js"></script>
<!-- waves js -->
<script src="files/assets/pages/waves/js/waves.min.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="files/bower_components/modernizr/js/modernizr.js"></script>
<script type="text/javascript" src="files/bower_components/modernizr/js/css-scrollbars.js"></script>
<!-- Custom js -->
<script type="text/javascript" src="files/assets/js/common-pages.js"></script>

<!-- notification js -->
<script type="text/javascript" src="files/assets/js/bootstrap-growl.min.js"></script>
<script type="text/javascript" src="files/assets/pages/notification/notification.js"></script>

<script>
function notify(message, type) {
  $.growl({
    message: message
  }, {
    type: type,
    allow_dismiss: false,
    label: 'Cancel',
    className: 'btn-xs btn-inverse',
    placement: {
      from: 'top',
      align: 'right'
    },
    delay: 5500,
    animate: {
      enter: 'animated fadeInRight',
      exit: 'animated fadeOutRight'
    },
    offset: {
      x: 30,
      y: 30
    }
  });
};


// Notification
<?php if ($this->session->flashdata('success') != '') { ?>
  notify('<?= $this->session->flashdata('success'); ?>', 'success');
  <?php } ?>
  <?php if ($this->session->flashdata('danger') != '') { ?>
    notify('<?= $this->session->flashdata("danger"); ?>', 'danger');
    <?php } ?>
    </script>
    <script>
    $(document).ready(function() {
        $('.form-control').addClass('fill');
    })
    </script>
  </body>

  </html>
