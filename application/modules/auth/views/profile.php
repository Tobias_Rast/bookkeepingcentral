<!-- [ breadcrumb ] start -->
<div class="page-header">
  <div class="page-block">
    <div class="row align-items-center">
      <div class="col-md-8">
        <div class="page-header-title">
          <h4 class="m-b-10"><?php echo fetchLine('Profile'); ?></h4>
        </div>
        <ul class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="<?= site_url() ?>">
              <i class="feather icon-home"></i>
            </a>
          </li>
          <li class="breadcrumb-item">
            <a href="#!"><?php echo fetchLine('Profile'); ?></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- [ breadcrumb ] end -->
<div class=" teal-gradient">
  <div class="pcoded-inner-content">
    <div class="main-body">
      <div class="page-wrapper">
        <div class="page-body">
          <!-- [ page content ] start -->
          <div class="row">
            <div class="card profile" style="margin:0 auto">
              <div class="card-header border-0 pb-0">
                <h5 class="card-header-text pb-3"><?php echo fetchLine('Edit Profile'); ?></h5>
                <div>
                  <p class="small mb-1"><?php echo fetchLine('Last login'); ?>: <span class="text-green"><?= date('F j, Y, g:i a', strtotime($user->last_login)) ?><span></p>
                    <p class="small"><?php echo fetchLine('Last update'); ?>: <span class="text-orange"><?= date('F j, Y, g:i a', strtotime($user->updated_date)) ?></span<</p>
                    </div>
                  </div>
                  <div class="card-block">
                    <hr>

                    <!-- end of view-info -->
                    <div class="edit-info" style="">

                      <div class="">
                        <form autocomplete="off" action="<?= site_url('auth/profile') ?>" method="post">
                          <input type="hidden" name="id" value="<?= $this->session->userdata('id') ?>">
                          <div class="general-info form-material">

                            <div class="material-group">
                              <div class="material-addone">
                                <i class="icofont icofont-user-alt-5"></i>
                              </div>
                              <div class="form-group form-primary">
                                <input type="text" name="name" class="form-control" required="" value="<?= $user->name ?>" autocomplete="off">
                                <span class="form-bar"></span>
                                <label class="float-label"><?php echo fetchLine('Name'); ?></label>
                              </div>
                            </div>
                            <div class="material-group">
                              <div class="material-addone">
                                <i class="icofont icofont-ui-email"></i>
                              </div>
                              <div class="form-group form-primary">
                                <input type="text" name="email" class="form-control" required="" value="<?= $user->email ?>" autocomplete="off">
                                <span class="form-bar"></span>
                                <label class="float-label"><?php echo fetchLine('Email'); ?></label>
                              </div>
                            </div>

                            <div class="material-group">
                              <div class="material-addone">
                                <i class="icofont icofont-ui-user"></i>
                              </div>
                              <div class="form-group form-primary">
                                <input type="text" name="username" class="form-control" required="" value="<?= $user->username ?>">
                                <span class="form-bar"></span>
                                <label class="float-label"><?php echo fetchLine('Username'); ?></label>
                              </div>
                            </div>


                            <div class="material-group">
                              <div class="material-addone">
                                <i class="icofont icofont-key"></i>
                              </div>
                              <div class="form-group form-primary">
                                <input type="text" name="password" class="form-control">
                                <!-- <span class="form-bar "><small><?php echo fetchLine('password_reset_info'); ?></small></span> -->
                                <small class="text-info-blue"><?php echo fetchLine('password_reset_info'); ?></small>
                                <label class="float-label"><?php echo fetchLine('Password'); ?></label>
                              </div>
                            </div>

                            <div class="text-center">
                              <hr class="mb-4 mt-3">
                              <input type="submit" class="btn btn-green waves-effect waves-light m-r-20" value="Save">
                            </div>
                          </div>
                        </form>
                        <!-- end of edit info -->
                      </div>
                      <!-- end of col-lg-12 -->

                    </div>
                    <!-- end of edit-info -->
                  </div>
                  <!-- end of card-block -->
                </div>                </div>
                <!-- [ page content ] end -->
              </div>
            </div>
          </div>
        </div>
      </div>
