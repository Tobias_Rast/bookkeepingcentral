<?php

class Auth_model extends CI_Model {

	public static $tbl_name = 'users';

	function __construct() {
		parent::__construct();
	}

	/* VALIDATE LOGIN CREDENTIALS */

	function validate( $username, $password ) {
		$encryption = $this->config->item( 'encryption_key' );
		//$salt =sha1(md5($encryption));
//        $password = md5($salt.$password);
		$password = hashPassword( $password );
		$this->db->where( " ( email = '$username' OR username = '$username' ) AND password ='$password'" );
		$this->db->where( "status", 1 );
		$query = $this->db->get( self::$tbl_name )->row();
		if ( $query ) {
			// Save into session
			if ( $query->role_id == 9 ) {
				$this->load->model( 'customers/Customers_model' );
				$this->load->model( 'users/Users_model' );
				$customer_id = $this->Customers_model->find( $query->id, 'user_id' );
				$language_id = $this->Users_model->find( $query->id, 'id' )->language_id;
				$data        = array(
					'cid'               => $query->id,
					'cname'             => $query->name,
					'cusername'         => $query->username,
					'cemail'            => $query->email,
					'ctoken'            => $query->token,
					'crole_id'          => $query->role_id,
					'clast_login'       => $query->last_login,
					'cavatar'           => $query->avatar,
					'customer_id'       => $customer_id->id,
					'user_language'     => $language_id,
					'subscription_date' => $customer_id->subscription_date,
					'c_logged_in'       => true,
				);
			} else {
				$data        = array(
					'id'               => $query->id,
					'name'             => $query->name,
					'username'         => $query->username,
					'email'            => $query->email,
					'token'            => $query->token,
					'role_id'          => $query->role_id,
					'last_login'       => $query->last_login,
					'avatar'           => $query->avatar,
					'logged_in'       => true,
				);
			}
			$this->session->set_userdata( $data );
			// Update login time
			$this->db->update( self::$tbl_name, array( 'last_login' => date( 'Y-m-d H:i:s' ) ), array( 'id' => $query->id ) );

			return true;
		} else {
			return false;
		}
	}

	public function isEmailExists( $email ) {
		$row = $this->db->select()->from( 'users' )->where( 'email', $email )->get()->row();
		if ( $row ) {
			return true;
		}
	}

	public function resetTokenInsert( $email, $reset_password_token ) {
		$this->db->update( 'users', array( 'reset_password_token' => $reset_password_token ), array( 'email' => $email ) );
	}

	public function resetPassword( $password, $email, $reset_password_token ) {
		$this->db->update( 'users', array( 'password' => hashPassword( $password ) ), array(
			'email'                => $email,
			'reset_password_token' => $reset_password_token
		) );

	}

	public function getCurrentUser() {
		$user = $this->db->get_where( 'users', [ 'id' => $this->session->userdata( 'id' ) ] )->row();

		return $user;
	}

	function updateProfile( $data ) {
		$user_id = $data['id'];
		if ( $user_id ) {
			$update_data = [
				'username'     => $data['username'],
				'name'         => $data['name'],
				'email'        => $data['email'],
				'updated_date' => date( 'Y-m-d H:i:s' ),

			];
			if ( $data['password'] != '' ) {
				$update_data['password'] = md5( $data['password'] );
			}

			return $this->db->update( 'users', $update_data, [ 'id' => $data['id'] ] );
		}

		return false;
	}
}
