<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MX_Controller {

    public function index() {
        /* Redirect if user is logged in */
        if ($this->session->userdata('logged_in')) {
            redirect('dashboard');
        }
        $this->load->model('Auth_model', 'auth');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username / Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        $this->form_validation->set_message('required', '%s required !');
        $this->form_validation->set_message('matches', '%s does not match !');

        if ($this->form_validation->run() == TRUE) {
            if ($user = $this->auth->validate($this->input->post('username'), $this->input->post('password'))) {
                redirect('/dashboard');
            } else {
                $this->session->set_flashdata('danger', 'Provided Credentials are not valid !!');
                redirect('/auth');
            }
        } elseif ($this->input->post()) {
            $this->session->set_flashdata('danger', stripErrors(validation_errors()));
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $data = array();
            $this->load->view('auth/login');
        }
    }

    public function forgotPassword() {
        $this->load->view('auth/forgot_password');
    }

    public function resetPassword() {

        $this->load->model('Auth_model', 'auth');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');

        $this->form_validation->set_message('required', '%s required !');
        $this->form_validation->set_message('matches', '%s does not match !');

        if ($this->form_validation->run() == TRUE) {
            $password = $this->input->post('password');
            $email = $this->input->post('email');
            $reset_token = $this->input->post('reset_password_token');

            $this->auth->resetPassword($password, $email, $reset_token);
            $this->session->set_flashdata('success', 'Password reset successfull.');
            redirect('auth');
        } elseif ($this->input->post()) {
            $this->session->set_flashdata('danger', stripErrors(validation_errors()));
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $data['email'] = $this->input->get('email');
            $data['reset_password_token'] = $this->input->get('reset_token');

            $this->load->view('auth/reset_password', $data);
        }
    }

    public function sendResetEmail() {
        $this->load->model('Auth_model', 'auth');
        $email = $this->input->post('email');
        $reset_token = md5(date('Y-m-d H:i:s'));

        $isExists = $this->auth->isEmailExists($email);
        if ($isExists) {

            sendForgotPasswordEmail($email, $reset_token);
            $this->auth->resetTokenInsert($email, $reset_token);

            $this->session->set_flashdata('success', 'Reset email sent.');
            redirect('auth');
        } else {
            $this->session->set_flashdata('danger', 'Email does not exists!');
            redirect('auth/forgotPassword');
        }
    }

    public function profile() {
        $this->load->model('Auth_model', 'auth');
        $data['user'] = $this->auth->getCurrentUser();

        $this->load->library('form_validation');


        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_validate_email');

        $this->form_validation->set_message('required', '%s required !');

        if ($this->form_validation->run($this) == TRUE) {
            if ($this->auth->updateProfile($this->input->post())) {
                $this->session->set_flashdata('success', 'Profile updated successfull.');
            }
            redirect($_SERVER['HTTP_REFERER']);
        } elseif ($this->input->post()) {
            $this->session->set_flashdata('danger', stripErrors(validation_errors()));
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['page'] = 'auth/profile';
        $this->load->view('template_admin', $data);
    }

    public function validate_email($str) {
        $emails = $this->db->select()->where('email',$str)->where('id !=',$this->session->userdata('id'))->get('users');
        if ($emails->num_rows() > 0) {
            $this->form_validation->set_message('validate_email', 'Email you entered is already been assigned.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function logout() {
	    $this->session->unset_userdata('logged_in');
	    redirect('auth', 'refresh');
    }

    public function forbidden() {
        echo '<h1>YOU ARE NOT ALLOWED TO VIEW THIS PAGE.<BR>CLICK BACK TO RETURN TO PREVIOUS PAGE</h1>';
    }

}
