<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Base_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('dashboard_model', 'dashboard');
    }

    public function index() {
        $data = array('page' => 'dashboard/dashboard');

        $this->load->view('template_admin', $data);
    }

}
