<!-- [ breadcrumb ] start -->
<?php
$crumbs = [
    [
        'page_title' => fetchLine('Dashboard'),
        'url' => base_url('dashboard')
    ],
    [
        'page_title' => fetchLine('Dashboard'),
        'url' => base_url('dashboard')
    ]
];
$this->load->view('inc/breadcrumb', array('crumbs' => $crumbs));
?>
<!-- [ breadcrumb ] end -->
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-body">
                <!-- [ page content ] start -->
                <div class="row">

                    <!-- amount start -->
                    <div class="col-md-6 col-xl-4">
                        <div class="card amount-card o-hidden">
                            <div class="card-block">
                                <h2 class="font-weight-bold">$23,567</h2>
                                <p class="text-muted f-w-600 f-16"><span
                                        class="text-c-blue">Amount</span> Processed</p>
                            </div>
                            <div id="amount-processed" style="height:50px"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-4">
                        <div class="card amount-card o-hidden">
                            <div class="card-block">
                                <h2 class="font-weight-bold">$14,552</h2>
                                <p class="text-muted f-w-600 f-16"><span
                                        class="text-c-green">Amount</span> Spent</p>
                            </div>
                            <div id="amount-spent" style="height:50px"></div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-4">
                        <div class="card amount-card o-hidden">
                            <div class="card-block">
                                <h2 class="font-weight-bold">$31,156</h2>
                                <p class="text-muted f-w-600 f-16"><span class="text-c-yellow">Profit</span>
                                    Processed
                                </p>
                            </div>
                            <div id="profit-processed" style="height:50px"></div>
                        </div>
                    </div>
                    <!-- amount end -->
                </div>
                <!-- [ page content ] end -->
            </div>
        </div>
    </div>
</div>
