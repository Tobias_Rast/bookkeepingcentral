<!-- Breadcrumb -->
<?php
$crumbs = [
    [
        'page_title' => fetchLine('Import'),
        'url' => base_url('import')
    ],
    [
        'page_title' => fetchLine('Auth'),
        'url' => base_url('auth')
    ]
];
$this->load->view('inc/breadcrumb', array('crumbs' => $crumbs));

?>
<div class="pcoded-inner-content">  
    <!-- Main-body start -->
    <div class="main-body">
        <div class="page-wrapper">
            <!-- Page-body start -->
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="card">
                            <div class="card-header border-0">
                                <h5><i class="fa fa-group"></i> <?= fetchLine('Import Timesheet'); ?></h5>
                            </div>
                            <div class="card-block">
                                <form action="<?= base_url( 'timesheet/importTimesheetToXero' ); ?>" method="POST" class="form-material">
                                
                                    <div class="row">
                                        <div class="col-sm-4">
                                            Number of new item uploaded to database: 
                                            <strong><?php echo $no_of_inserted; ?></strong>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            Number of duplicate item uploaded: 
                                            <strong><?php echo $no_of_duplicate; ?></strong>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 text-left">
                                        <hr class="my-3">
                                        <button type="submit" class="btn btn-green"><i class="icofont icofont-save"></i> <?php echo fetchLine('Import Timesheet to Xero'); ?></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-block">
                                <div class="dt-responsive table-responsive">
                                  <table id="simpletable" class="table table-striped table-bordered nowrap">
                                    <thead>
                                      <tr>
                                        <th><?php echo fetchLine('ID'); ?></th>
                                     
                                        <?php foreach ($column_mapping as $key => $col) : ?>
                                            <th><?php echo $col['label']; ?></th>
                                        <?php endforeach; ?>
                                        <th><?php echo fetchLine('Upload Date'); ?></th>
                                        <th><?php echo fetchLine('Payperiod Start'); ?></th>
                                        <th><?php echo fetchLine('Payperiod End'); ?></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <?php if($timesheets){
                                        foreach($timesheets as $timesheet) {
                                          ?>
                                          <tr>
                                            <td><?= $timesheet->id; ?></td>
                                            <?php foreach ($column_mapping as $key => $col) : ?>
                                                <td><?= $timesheet->$key; ?></td>
                                            <?php endforeach; ?>
                                            <th><?= $timesheet->upload_date; ?></th>
                                            <th><?= $timesheet->payperiod_start; ?></th>
                                            <th><?= $timesheet->payperiod_end; ?></th>
                                          </tr>
                                        <?php }
                                      } ?>
                                    </tbody>
                                  </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Page-body end -->
        </div>
    </div>
    <!-- Main-body end -->
</div>