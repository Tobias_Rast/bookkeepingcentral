<!-- Breadcrumb -->
<?php
$crumbs = [
    [
        'page_title' => fetchLine('Timesheet'),
        'url' => base_url('timesheet')
    ],
    [
        'page_title' => fetchLine('Manage Timesheet'),
        'url' => base_url('timesheet')
    ]
];
$this->load->view('inc/breadcrumb', array('crumbs' => $crumbs));

?>
<div class="pcoded-inner-content">  
    <!-- Main-body start -->
    <div class="main-body">
        <div class="page-wrapper">
            <!-- Page-body start -->
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">
                        
                        <div class="card">
                            <div class="card-header border-0">
                                <h5><i class="fa fa-group"></i> <?php echo fetchLine('Timesheet'); ?></h5>
                            </div>
                            <div class="card-block">
                                <?php echo validation_errors('<p class="form_error">','</p>'); ?>
                                <form action="<?= base_url('timesheet/upload'); ?>" method="POST" enctype="multipart/form-data" class="form-material" accept-charset="utf-8">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group form-primary">
                                                <input type="file" name="timesheetFile" id="timesheetFile" class="form-control filestyle fill" value="" data-icon="false" required="">
                                            </div>
                                            <div class="form-group form-primary">
                                                <label>Period Start Date (Monday)</label>
                                                <input type="date" class="form-control" required="" name="date_start" value="<?=set_value('date_start')?>">
                                            </div>
                                            <div class="form-group form-primary">
                                                <label>Period End Date (Sunday)</label>
                                                <input type="date" class="form-control" required="" name="date_end" value="<?=set_value('date_end')?>">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <hr class="my-3">
                                        </div>
                                        <div class="col-sm-12 text-left">
                                            <input type="submit" name="uploadFile" value="UPLOAD" class="btn btn-green"/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Page-body end -->
        </div>
    </div>
    <!-- Main-body end -->
</div>