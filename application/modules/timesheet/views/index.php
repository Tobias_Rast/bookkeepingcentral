<!-- Breadcrumb -->
<?php
$crumbs = [
    [
        'page_title' => fetchLine('Timesheet'),
        'url' => base_url('timesheet')
    ],
    [
        'page_title' => fetchLine('Manage Timesheet'),
        'url' => base_url('timesheet')
    ]
];
$this->load->view('inc/breadcrumb', array('crumbs' => $crumbs));

?>
<div class="pcoded-inner-content">  
    <!-- Main-body start -->
    <div class="main-body">
        <div class="page-wrapper">
            <!-- Page-body start -->
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- Zero config.table start -->
                        <div class="card">
                          <div class="card-header border-0">
                            <h5><i class="fa fa-group"></i> <?php echo fetchLine('Timesheets'); ?></h5>
                            <span class="d-inline-block pull-right">
                              <?php if ($this->acl->has_permission('users-users-addUser')) { ?>
                                <a class="btn btn-info" href="<?= site_url('timesheet/upload'); ?>"><i class="icofont icofont-plus"></i><?php echo fetchLine('Upload Timesheet'); ?></a>
                              <?php } ?>
                            </span>
                          </div>
                          <div class="card-block">
                            <div class="dt-responsive table-responsive">
                              <table id="base-style" class="table table-striped table-bordered nowrap dataTable" role="grid" aria-describedby="base-style_info">
                                <thead>
                                  <tr>
                                    <th><?php echo fetchLine('Email'); ?></th>
                                    <th><?php echo fetchLine('Date'); ?></th>
                                    <th><?php echo fetchLine('Imported to Xero'); ?></th>
                                    <th><?php echo fetchLine('Import Status'); ?></th>
                                    <?php foreach ($column_mapping as $key => $col) : ?>
                                        <th><?php echo $col['label']; ?></th>
                                    <?php endforeach; ?>
                                    <th><?php echo fetchLine('Upload Date'); ?></th>
                                    <th><?php echo fetchLine('Payperiod Start'); ?></th>
                                    <th><?php echo fetchLine('Payperiod End'); ?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php if($timesheets){
                                    foreach($timesheets as $timesheet) {
                                      ?>
                                      <tr>
                                        <td><?= $timesheet->email; ?></td>
                                        <td><?= $timesheet->date; ?></td>
                                        <td><?= $timesheet->processed ? 'True' : 'False' ; ?></td>
                                        <td><?= $timesheet->import_status; ?></td>
                                        <?php foreach ($column_mapping as $key => $col) : ?>
                                            <td><?= $timesheet->$key; ?></td>
                                        <?php endforeach; ?>
                                        <th><?= $timesheet->upload_date; ?></th>
                                        <th><?= $timesheet->payperiod_start; ?></th>
                                        <th><?= $timesheet->payperiod_end; ?></th>
                                      </tr>
                                    <?php }
                                  } ?>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        <!-- Zero config.table end -->

                    </div>
                </div>
            </div>
            <!-- Page-body end -->
        </div>
    </div>
    <!-- Main-body end -->
</div>