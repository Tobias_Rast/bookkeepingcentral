<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TimesheetModel extends CI_Model {

    public function batchImportTimesheet($data) {
        $this->db->insert_batch('timesheets', $data);
    }

    public function insertTimesheet($data, $start, $end) {
        $data['upload_date'] = date('Y-m-d H:i:s');
        $data['payperiod_start'] = $start;
        $data['payperiod_end'] = $end;
        $this->db->insert('timesheets', $data);
    }

    public function getTimesheets($toImport = false){
        if ($toImport) {
            $this->db->where('processed', 0);
        }

        $query = $this->db->get('timesheets');
        $results = $query->result();
        return $results;
    }

    public function recordExist($email, $date) {
        $this->db->where('email', $email);
        $this->db->where('date', $date);
        $query = $this->db->get('timesheets');
        $count = $query->num_rows();
        if ($count > 0){
            return true;
        } else {
            return false;
        }
    }

    public function getRequiredColumns() {
        $this->db->select('excel_column');
        $this->db->where('required', 1);
        $query = $this->db->get('mappings');
        $result = $query->result_array();
        return $result;
    }

    public function getMapping() {
        $query = $this->db->get('mappings');
        $result = $query->result();
        return $result;
    }

    public function getExcelColumns() {
        $this->db->select('excel_column');
        $query = $this->db->get('mappings');
        $result = $query->result_array();
        return $result;
    }

    public function insertPayItem($data) {
        $exists = $this->payItemExist($data['EarningsRateID']);
        if (!$exists) {
            $item = [
                'name' => $data['Name'],
                'account_code' => $data['AccountCode'],
                'type_of_units' => $data['TypeOfUnits'],
                'is_exempt_from_tax' => $data['IsExemptFromTax'],
                'is_exempt_from_super' => $data['IsExemptFromSuper'],
                'earnings_type' => $data['EarningsType'],
                'earnings_rate_id' => $data['EarningsRateID'],
                'rate_type' => $data['RateType']
            ];
            $this->db->insert('payitems', $item); 
        }
    }

    public function payItemExist($rateId) {
        $this->db->where('earnings_rate_id', $rateId);
        $query = $this->db->get('earning_rates');
        $count = $query->num_rows();
        if ($count > 0){
            return true;
        } else {
            return false;
        }
    }

    public function getEarningRateIds() {
        $this->db->select('m.label, m.table_key, e.earnings_rate_id');
        $this->db->from('mappings m');
        $this->db->join('earning_rates e', 'm.rate_id = e.id', 'left');
        $this->db->where('m.is_rate', 1);
        $query = $this->db->get(); 
        $result = $query->result();
        return $result;
    }

    public function flagTimesheetAsImported($ids, $status) {
        $data = array(
            'processed' => 1, 
            'import_status' => $status,
            'import_date' => date('Y-m-d')
        );

        $this->db->where_in('id', $ids);
        $this->db->update('timesheets', $data);
    }
    

}
