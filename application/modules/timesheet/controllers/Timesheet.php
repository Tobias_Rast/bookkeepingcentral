<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use XeroPHP\Application\PrivateApplication;
use XeroPHP\Models\PayrollAU\Timesheet\TimesheetLine;
use XeroPHP\Models\PayrollAU\Timesheet as XeroTimesheet;

class Timesheet extends Base_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('TimesheetModel', 'timesheet');
        $this->load->model('employee/EmployeeModel', 'employee');
    }

    public function index() {
        $data['page'] = 'timesheet/index';
        $data['timesheets'] = $this->timesheet->getTimesheets();
        $data['column_mapping'] = $this->getMapping();

        //initial table population. check where to put this
        //$this->importPayItems();
        //$this->importEmployee(true);
 
        $this->load->view('template_admin', $data);
    }

    public function upload() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('timesheetFile', '', 'callback_file_check');
        $this->form_validation->set_rules('date_start', '', 'trim|exact_length[10]|callback_date_start_check');
        $this->form_validation->set_rules('date_end', '', 'trim|exact_length[10]|callback_date_end_check');
            
        if ($this->input->post('uploadFile')) {
            if($this->form_validation->run($this) == true){
                $this->uploadTimesheet();
            } else {
				$data = array('page' => 'timesheet/upload');
        		$this->load->view('template_admin', $data);	
			}
        } else {

            // Update employee database 
            $this->importEmployee();
			
			$data = array('page' => 'timesheet/upload');
        	$this->load->view('template_admin', $data);

        }        
    }

    private function callXero() {
        $config = [
            'oauth' => [
                'callback' => $this->config->item('zero_callback_url'),
                'consumer_key' => $this->config->item('zero_consumer_key'),
                'consumer_secret' => $this->config->item('zero_consumer_secret'),
                'rsa_private_key' => $this->config->item('xero_server_key'),
            ],
			'curl'  => [
			            CURLOPT_SSL_VERIFYPEER => false
        	]
        ];
        $xero = new PrivateApplication($config);
        return $xero;
    }

    private function importEmployee($createAll = false) {
        $today = new DateTime('now', new DateTimeZone('UTC'));
        $today = $today->format('Y,m,d');
        $xero = $this->callXero();

        if ($createAll) {
            $employees = $xero->load('PayrollAU\\Employee')->execute();
        } else {
            $employees = $xero->load('PayrollAU\\Employee')
                ->where('UpdatedDateUTC >= DateTime('.$today.')')
                ->execute();
        }
        
        $items = json_decode(json_encode($employees, JSON_FORCE_OBJECT),true);
        if (!empty($items)) {
            foreach($items as $key => $item) {
                $this->employee->saveEmployee($item);
            }
        }
    }

    private function importPayItems() {
        $xero = $this->callXero();
        $payitems = $xero->load('PayrollAU\\PayItem')->execute();
        $rates = json_decode(json_encode($payitems, JSON_FORCE_OBJECT),true);
        $items = $rates[0]['EarningsRates'];
        foreach($items as $key => $item) {
            $this->timesheet->insertPayItem($item);
        }
    }

    public function file_check(){
        if (empty($_FILES['timesheetFile']['name']) || $_FILES['timesheetFile']['name'] == "") {
            $this->form_validation->set_message('file_check', 'File upload is required.'); 
        }
        $extension = pathinfo($_FILES['timesheetFile']['name'], PATHINFO_EXTENSION);
        $extension = strtolower($extension);
        if ($extension != 'xlsx' && $extension != 'xls') {
            $this->form_validation->set_message('file_check', 'Invalid file uploaded.');
            return false;
        }
    }

    public function date_start_check($date){
        if ($date) {
            $date = date('N', strtotime($date));
            if ($date != 1) {
                $this->form_validation->set_message('date_start_check', 'The Period Start Date must fall on Monday.');
                return false;
            }
        } else {   
            $this->form_validation->set_message('date_start_check', 'The Period Start Date field must contain a valid date.');
            return false;
        }
    }

    public function date_end_check($date){
        $start = $this->input->post('date_start');

        if ($date && $start) {
            $end = date('N', strtotime($date));
            if ($end != 7) {
                $this->form_validation->set_message('date_end_check', 'The Period End Date must fall on Sunday.');
                return false;
            }

            // temp, how to in ci?
            $date_start = date_create($start);
            $date_end = date_create($date);
            $diff = date_diff($date_start, $date_end); 
            $days = $diff->format("%d");
            if (!$days || $days != '6') {
                $this->form_validation->set_message('date_end_check', 'Payperiod of 7 days is required.');
                return false;
            }

        } else {   
            $this->form_validation->set_message('date_end_check', 'The Period End Date field must contain a valid date.');
            return false;
        }
    }

    public function uploadTimesheet() {

        $path = "./uploads/timesheets/";
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'xlsx|xls';
        $config['file_ext_tolower'] = TRUE;
        $config['remove_spaces'] = TRUE;

        // mapping
        $timesheetCols = $this->getMapping();
        $colArray = $this->getExcelColumns();
        $requiredHeader = $this->config->item('employee_map_key');

        // period dates
        $payperiod_start = $this->input->post('date_start');
        $payperiod_end = $this->input->post('date_end'); 

        $this->load->library('upload', $config);
        $this->load->library('excel');

        $this->upload->do_upload('timesheetFile');
        $data['upload_data'] = $this->upload->data(); 
        if (!empty($data['upload_data']['file_name'])) {
            $import_xls_file = $data['upload_data']['file_name'];
        } else {
            $import_xls_file = 0;
        }

        $inputFileName = $path . $import_xls_file;
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                    . '": ' . $e->getMessage());
        }

        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);    
        $arrayCount = count($allDataInSheet);
        $excelArr = array_combine($colArray, $colArray);

        $SheetDataKey = array();
        foreach ($allDataInSheet as $dataInSheet) {
            foreach ($dataInSheet as $key => $value) {   
                if (in_array(trim($value), $colArray)) {
                    $value = preg_replace('/\s+/', '', $value);
                    $test = (string)$value;
                    $SheetDataKey[trim($value)] = $key;
                }
            }
        }

        $SheetDataKey = array();

        // get first row header
        if ($allDataInSheet[1]) {
            $collheader = $allDataInSheet[1];
            $headerNames = array_values($collheader);
            if (in_array($requiredHeader, $headerNames)) {
                foreach ($collheader as $key => $value) {
                    if (in_array(trim($value), $colArray)) {
                        $value = preg_replace('/\s+/', '', $value);
                        $test = (string)$value;
                        $SheetDataKey[trim($value)] = $key;                
                    }
                }
            } 
        }

        if (!empty($SheetDataKey)) {
            $reqKey = $SheetDataKey[$requiredHeader];
            for ($i = 2; $i <= $arrayCount; $i++) {
                $reqVal = trim($allDataInSheet[$i][$reqKey]);
                if (!$reqVal or $reqVal=='') continue;

                $fetchDataItem = [];
                foreach ($timesheetCols as $key => $timesheet) {
                    $tblKey = $key;
                    $sheetKey = $timesheet['excel'];

                    if (!array_key_exists($sheetKey, $SheetDataKey)) {
                        continue;
                    }
                    $sheetData  = $SheetDataKey[$sheetKey];
                    if (!$allDataInSheet[$i][$sheetData]) continue;
                    $type = $timesheet['type'];
                    $sheetValue = $this->sanitizeSheetValue($type, $allDataInSheet[$i][$sheetData]);
                    $fetchDataItem[$key] = $sheetValue;
                }
                if (!empty($fetchDataItem)) {
                    $fetchData[] = $fetchDataItem;
                }
            }

            $importResult = $this->importTimesheetToDatabase($fetchData, $payperiod_start, $payperiod_end);
            $data['no_of_duplicate'] = $importResult['duplicate'];
            $data['no_of_inserted'] = $importResult['inserted'];
        }

        $data['timesheets'] = $this->timesheet->getTimesheets(true);
        $data['column_mapping'] = $this->getMapping();
        $data['page'] = 'timesheet/import';
        $this->load->view('template_admin', $data);
    }

    private function getRequiredColumns() {
        $columns = $this->timesheet->getRequiredColumns();
        $result = array_column($columns, 'excel_column');
        return $result;
    }

    private function getMapping() {
        $columns = $this->timesheet->getMapping();
        $results = [];
        foreach ($columns as $column) {
            $key = $column->table_key;
            $results[$key] = [
                'label' => $column->label, 
                'excel' => $column->excel_column,
                'type'  => $column->type
            ];
        }
        return $results;
    }

    private function getExcelColumns() {
        $columns = $this->timesheet->getExcelColumns();
        $result = array_column($columns, 'excel_column');
        return $result;
    }
        
    private function sanitizeSheetValue($type,$data) {
        if ($type == 'text') {
            $output = filter_var(trim($data), FILTER_SANITIZE_STRING);
        } else if ($type == 'email') {
            $output = filter_var(trim($data), FILTER_SANITIZE_EMAIL);
        } else if ($type == 'float') {
            $output = filter_var($data, FILTER_VALIDATE_FLOAT);
        } else if ($type == 'date' && $data!='') {
            $date = DateTime::createFromFormat('d/m/Y', $data);
            $output = $date->format('Y-m-d 00:00:00');
        } else {
            $output = '';
        }
        return $output;
    }

    private function importTimesheetToDatabase($items, $period_start, $period_end) {
        $date_start = date_create($period_start);
        $date_end = date_create($period_end);
        $date_start = date_format($date_start, 'Y-m-d 00:00:00');
        $date_end = date_format($date_end, 'Y-m-d 00:00:00');
        $inserted = 0;
        $duplicate = 0;
        foreach ($items as $key => $item) {
            $exist = $this->timesheet->recordExist($item['email'], $item['date']);
            if (!$exist) {
                $this->timesheet->insertTimesheet($item, $date_start, $date_end);
                $inserted++;
            } else {
                $duplicate++;
            }
        }
        $result = ['duplicate'=>$duplicate, 'inserted'=>$inserted];
        return $result;
    }

    public function importTimesheetToXero(){
        $timesheets = $this->timesheet->getTimesheets(true);
        if (!empty($timesheets)) {
            $employeeTimesheets = $this->groupTimesheetByEmployeeId($timesheets);
            $this->createTimesheets($employeeTimesheets);
            $this->session->set_flashdata('success', 'Timesheets uploaded to Xero successfuly!');
        } else {
            $this->session->set_flashdata('success', 'No timesheets to import.');
        }

        redirect('timesheet', 'refresh');
    }

    private function getXeroTimesheetId($employeeID, $startDate) {
        $dateUTC = new DateTime($startDate, new DateTimeZone('UTC'));
        $date = $dateUTC->format('Y,m,d');

        $xero = $this->callXero();
        $result = $xero->load("\\XeroPHP\\Models\\PayrollAU\\Timesheet")
            ->where('EmployeeID=GUID("'.$employeeID.'")')
            ->where('StartDate = DateTime('.$date.')')
            ->execute();
        $item = json_decode(json_encode($result, JSON_FORCE_OBJECT),true);
        return $item;
    }

    private function createXeroTimesheetId($employeeID, $startDate, $endDate) {
        $xero = $this->callXero();
        $timesheet = new XeroTimesheet($xero);
        $timesheet->setEmployeeID($employeeID)
            ->setStartDate(new DateTime($startDate))
            ->setEndDate(new DateTime($endDate))
            ->setStatus('Draft');
        $timesheet->save();
    }

    private function createDateRange($startDate, $endDate, $format = 'Y-m-d') {
        $begin = new DateTime($startDate);
        $end = new DateTime($endDate);

        $interval = new DateInterval('P1D');
        $end->add($interval); 
        $dateRange = new DatePeriod($begin, $interval, $end);

        $range = [];
        foreach ($dateRange as $date) {
            $range[] = $date->format($format);
        }
        return $range;
    }

    private function groupTimesheetByEmployeeId($timesheets) {
        if (!$timesheets || empty($timesheets)) {
            return $results;
        }

        // map earning rates id
        $rateIds = $this->timesheet->getEarningRateIds();
        $earningRates = [];
        foreach ($rateIds as $key => $rate) {
            $earningRates[$rate->table_key] = $rate->earnings_rate_id;
        }

        $items = [];
        $failed_ids = [];
        $no_of_rates_key = $this->config->item('no_of_rates_key');

        foreach($timesheets as $key => $timesheet) {
            if (!$timesheet->email || !$timesheet->date) {
                $failed_ids[] = $timesheet->id;
                continue;
            }

            $email = trim($timesheet->email);

            $dateUTC = new DateTime($timesheet->date, new DateTimeZone('UTC'));
            $date = $dateUTC->format('Y-m-d');

            // date specified invalid
            if (!$date) {
                $failed_ids[] = $timesheet->id;
                continue;
            }

            $startUTC = new DateTime($timesheet->payperiod_start);
            $endUTC = new DateTime($timesheet->payperiod_end);
            $start = $startUTC->format('Y-m-d');
            $end = $endUTC->format('Y-m-d');

           
            // item is not within specified payperiod
            if (!($date >= $start && $date <= $end)) {
                $failed_ids[] = $timesheet->id;
                continue;
            }

            for ($i = 1; $i<= $no_of_rates_key; $i++) {
                $rateKey = 'rate'.$i;
                $rateId = $earningRates[$rateKey];
                $items[$email]['rates'][$rateKey]['earnings_rate_id'] = $rateId;
                $items[$email]['rates'][$rateKey]['dates'][$date] = $timesheet->$rateKey;
            }
                
            $items[$email]['ids'][] = $timesheet->id;
            $items[$email]['payperiod']['start'] = $start;
            $items[$email]['payperiod']['end'] = $end;

        }

        // final cleanup
        $results = [];
        foreach($items as $email => $item) {
            $emp = $this->employee->getEmployeeID($email);

            // employee not found, mark as the ids as failed import
            if (!$emp || empty($emp)) {
                $failed_ids[] = array_merge($item['ids'], $failed_ids);
                continue;
            }

            $start = $item['payperiod']['start'];
            $end = $item['payperiod']['end'];
        
            $daterange = $this->createDateRange($start, $end);

            // timesheet id differs per employee and earning rates
            $rates = $item['rates'];
            foreach ($rates as $key => $rate) {
                
                // deletes empty rates value
                // incase empty earning rate for employee needs to be uploaded,
                // comment out this line
                $rate_dates = $rate['dates'];
                $tmp = array_filter($rate_dates);
                if (!array_filter($rate_dates)) {
                    unset($item['rates'][$key]);
                    continue;
                }

                $rate_dates_arr = array_keys($rate_dates);
                $dates_diff = array_diff($daterange,$rate_dates_arr);

                //add default 0 value on missing date
                //to prevent error when creating xero timesheet
                if (!empty($dates_diff)) {
                    foreach($dates_diff as $dkey => $date) {
                        $item['rates'][$key]['dates'][$date] = 0;
                    }
                    ksort($item['rates'][$key]['dates']);
                }
            }

            // Get timesheet payperiod id
            if (!isset($results[$emp->Id]['payperiod']['guid'])) {
                $timesheetInfo = $this->getXeroTimesheetId($emp->EmployeeID, $start);
                if (!empty($timesheetInfo)) {
                    $results[$emp->Id]['payperiod']['guid'] = $timesheetInfo[0]['TimesheetID'];
                } else {
                    $this->createXeroTimesheetId($emp->EmployeeID, $start, $end);
                    $timesheetInfo = $this->getXeroTimesheetId($emp->EmployeeID, $start);
                    if ($timesheetInfo) {
                        $results[$emp->Id]['payperiod']['guid'] = $timesheetInfo[0]['TimesheetID'];
                    }
                }
            }

            $results[$emp->Id]['payperiod']['start'] = $start;
            $results[$emp->Id]['payperiod']['end'] = $end;
            $item['payperiod'] = $results[$emp->Id]['payperiod'];
            $item['employee_guid'] = $emp->EmployeeID;
            $results[$emp->Id] = $item; 
        }

        $timesheet = [
            'payitems' => $results,
            'failed_ids' => $failed_ids,
        ];

        return $timesheet;
    }


    private function createTimesheets($items) {
        $xero = $this->callXero();

        if (!empty($items['failed_ids'])) {
            $this->timesheet->flagTimesheetAsImported($items['failed_ids'], 'Failed');
        }

        $itemsToImport = [];
        foreach ($items['payitems'] as $id => $item) {
            if (!isset($item['payperiod']) || empty($item['rates']) || !isset($item['employee_guid'])) {
                $this->timesheet->flagTimesheetAsImported($item['ids'], 'Failed');
                continue;
            }

            $rates = $item['rates'];
            $ts = $item['payperiod'];
            $employee_guid = $item['employee_guid'];

            $timesheet = new XeroTimesheet($xero);
            $timesheet->setTimesheetID($ts['guid'])
                ->setEmployeeID($employee_guid)
                ->setStartDate(new DateTime($ts['start']))
                ->setEndDate(new DateTime($ts['end']));

            foreach ($rates as $key => $rate) {
                $earning_id = $rate['earnings_rate_id'];
                $timesheetLines = new TimesheetLine($xero);
                foreach ($rate['dates'] as $date => $unit) {
                    $timesheetLines->setEarningsRateID($earning_id)
                        ->setTrackingItemID(NULL)
                        ->addNumberOfUnit($unit);
                }
                $timesheet->addTimesheetLine($timesheetLines);                
            }

            $itemsToImport[] = $timesheet;
            $this->timesheet->flagTimesheetAsImported($item['ids'], 'Success');
        }

        if (!empty($itemsToImport)) {
            $xero->saveAll($itemsToImport);
        }
    }

}
