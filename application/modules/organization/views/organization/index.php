<!-- Breadcrumb -->
<?php
$crumbs = [
    [
        'page_title' => fetchLine('Organization'),
        'url' => base_url('organization')
    ],
    [
        'page_title' => fetchLine('Auth'),
        'url' => base_url('organization')
    ]
];
$this->load->view('inc/breadcrumb', array('crumbs' => $crumbs));

?>
<div class="pcoded-inner-content">
    <!-- Main-body start -->
    <div class="main-body">
        <div class="page-wrapper">
            <!-- Page-body start -->
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Zero config.table start -->
                        <div class="card">
                            <div class="card-header border-0">
                                <h5><i class="fa fa-briefcase"></i> <?= fetchLine('Organization detail'); ?></h5>
                                <span class="d-inline-block pull-right">

                                </span>
                            </div>
                            <div class="card-block"><pre>
                                <?php
                                //These are the minimum settings - for more options, refer to examples/config.php
                                $config = [
                                    'oauth' => [
                                        'callback' => $this->config->item('xero_callback_url'),
                                        'consumer_key' => $this->config->item('xero_consumer_key'),
                                        'consumer_secret' => $this->config->item('xero_consumer_secret'),
                                        'rsa_private_key' => $this->config->item('xero_server_key'),
                                    ],
                                ];

//                                $xero = new PrivateApplication($config);
                                $xero = new \XeroPHP\Application\PrivateApplication($config);
                                $organization = $xero->load('Accounting\\Organisation')->execute();

                                $organization_data = json_decode(json_encode($organization, JSON_FORCE_OBJECT),true);

                               
                                foreach ($organization_data[0] AS $key=>$org){
                                    echo $key.'---'.$org.'<br>';
                                }
    

                                

                                ?>
                            </pre></div>
                        </div>
                        <!-- Zero config.table end -->
                    </div>
                </div>
            </div>
            <!-- Page-body end -->
        </div>
    </div>
    <!-- Main-body end -->
</div>