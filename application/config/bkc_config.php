<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$ci = get_instance();

$config['class'] = $ci->router->class;
$config['method'] = $ci->router->method;

$config['primary_nav'] = array(
    array(
        'name' => 'Dashboard',
        'icon' => 'fa fa-tachometer-alt',
        'url' => 'dashboard',
        'permission' => 'dashboard-index',
    ),
    array(
        'name' => 'Employee',
        'icon' => 'fa fa-group',
        'url' => 'employee',
        'permission' => 'employee'
    ),
    array(
        'name' => 'Timesheet',
        'icon' => 'fa fa-list',
        'url' => 'timesheet',
        'permission' => 'timesheet'
    ),
    array(
        'name' => 'Configuration',
        'icon' => 'fa fa-wrench',
        'url' => 'rate',
        'permission' => 'rate-rate-index',
        'sub' => array(
            array(
                'name' => 'Earning Rates',
                'url' => 'rate',
                'permission' => 'rate-rate-index'
            ),
            array(
                'name' => 'Mappings',
                'url' => 'rate/mapping',
                'permission' => 'rate-mapping-index'
            )
        )
    ),
    array(
        'name' => 'User Settings',
        'icon' => 'fa fa-user',
        'url' => 'users',
        'permission' => 'users-users-index',
        'sub' => array(
            array(
                'name' => 'Users',
                'icon' => 'fa fa-user',
                'url' => 'users',
                'permission' => 'users-users-index'
            ),
            array(
                'name' => 'Permissions',
                'icon' => 'fa fa-dashboard',
                'url' => 'users/permissions',
                'permission' => 'users-permissions-index'
            ),
            array(
                'name' => 'Roles',
                'icon' => 'fa fa-dashboard',
                'url' => 'users/roles',
                'permission' => 'users-roles-index'
            )
        )
    ),
);

$config['template'] = array(
    'title' => SITE_NAME,
    'author' => 'Pegotec Pte. Ltd.',
    'description' => 'Control panel for ' . SITE_NAME,
    'keywords' => SITE_NAME,
    'active_module' => $ci->router->fetch_module(),
    'active_page' => $config['class'],
    'active_method' => $config['method'],
);

$config['smtp_email_address'] = 'sujeet@pegotec.net';
$config['smtp_password'] = '***********';
$config['smtp_host'] = 'smtp.gmail.com';
$config['smtp_port'] = '465';
$config['smtp_encryption'] = 'ssl';

$config['xero_server_key'] = "-----BEGIN RSA PRIVATE KEY-----
MIICXgIBAAKBgQDW5eNZHP6Wy4gY9eTrFj+gMHzsd4DDEuxGU+Kd+0AXeAIqZImi
Gmfc1dU+PbNHtJEfCzltfKZHgtSXY8kdJlXzpx+Gi5LObLGwYPt+mDAKU6i8kNI7
+aNT6VMohQYsUHvA+EKPE2rMvFvvK6FX2QwFlnSr9CJtHEU++MlDGvi69wIDAQAB
AoGAdT6/p2L2U/d/rX6bAtgEprVNswYQLl4oosYpod/uu98x70cjAFo/yHCyA19O
bRTLCJDrKB0V63Yp3t8GXu0HfwPhAglJ+UELECUbKrbUEttCG3Bpf8hT5ykmqDbm
H0EK9/QNbE7JFvV88EciTKa6hCfSH2NQ5lHSF2QgjeEJvqECQQDyQFxBYvyKAecy
FkkkphRUhvUmOULlZvAlTaaOxtFJ6+bAfzm4a4dCX7TOusHFSSBxr0KxSVNIQN2e
9tuuoz6DAkEA4xgdEity11fxY/euk/68o2ymQ3h3gVKH7bwgPGyAzkzGF4jof7On
M5BVe6sxAR+ifCjg5xaMKZYOT9PSUcnnfQJBAK7jWfb2kFzNrOwS/LERmSHA4KC8
vBfDLeGwYms3C4bzYH5eGvyR3G9FitGoMBXOijaokVxOFGrYMWb+znAQeCMCQQCK
oD+uxcMW3WJHifyR7yCsCjj8Wt9onD7JyttKx1lhldAesb5rpfldKrBkC76gVvVT
IYkYx9TL1gPqiv2KW9vpAkEAmpFsVhSnu0ARb4CIqEfqDm9eNy9sZVaxDLchuVSd
oHcDfiMOZsgx26mH1C6Qc6upullC/3iLaBjV+XZU+KmfrA==
-----END RSA PRIVATE KEY-----";

$config['zero_callback_url'] = 'http://localhost.bookkeepingcentral/timesheet';
$config['zero_consumer_key'] = 'ZPTHGIP8RGH4CGANBAFYDB6MZ4UUAY';
$config['zero_consumer_secret'] = 'VQNVUAIWOKXVOYH4Z0A8TY3JUDZP7C';

$config['uploads_timesheet']  = './uploads/timesheets/';
$config['employee_map_key'] = 'Email'; 
$config['no_of_rates_key'] = 15;


$config['base_url'] = 'http://localhost.bookkeepingcentral';
$config['log_threshold'] = 4; 
$config['log_path'] = '/var/log/ci_logs/';
