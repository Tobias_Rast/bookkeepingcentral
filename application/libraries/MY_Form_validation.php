<?php  
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {

    function run($module = '', $group = '') {
       (is_object($module)) AND $this->CI = &$module;
        return parent::run($group);
    }

	/**
	* sets the error message associated with a particular field
	*
	* @param   string  $field  Field name
	* @param   string  $error  Error message
	* used in controller like this: 
	* $this->form_validation->setError('username', 'Invalid login credentials');
	*/
    public function setError($field, $error) {
    	$this->_error_array[$field] = $error;
   	}
}