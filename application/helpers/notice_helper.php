<?php

if (!function_exists('sendNotice')) {

    /**

     * Param: $data (array)
     * $data keys : notice_key, customer_id
     *      */
    function sendNotice($data) {
        $ci = & get_instance();

        if ($data['notice_key'] && $data['customer_id']) {
            $notice = $ci->db->select()->from('notices')->where('key', $data['notice_key'])->get()->row();
            $notice_data = [
                'customer_id' => $data['customer_id'],
                'notice_id' => $notice->id,
                'subject' => $notice->subject,
                'subject_fr' => $notice->subject_fr,
                'description' => $notice->description,
                'description_fr' => $notice->description_fr,
                'created_date' => date('Y-m-d H:i:s')
            ];
            if ($ci->db->insert('customer_notices', $notice_data)) {
                return true;
            }
        }
        return false;
    }

}