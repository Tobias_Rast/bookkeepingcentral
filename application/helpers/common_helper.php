<?php

function flash($index = 'msg') {
    $ci = &get_instance();
    if ($ci->session->flashdata($index)) {
        return $ci->session->flashdata($index);
    } else {
        return false;
    }
}

function get_userdata($index) {
    $ci = &get_instance();

    return $ci->session->userdata($index) ? $ci->session->userdata($index) : false;
}

function debug($data, $die = true) {
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    if ($die) {
        die;
    }
}

/*
 * Create system log
 * $table: table that is updated
 * $id: row id
 * $data: New table data that have been updated
 *  */
if (!function_exists('createSystemLog')) {

    function createSystemLog($table, $id, $data) {
        $ci = & get_instance();
        if ($id && $table) {
            $old_data = $ci->db->get_where($table, ['id' => $id])->row();
            $new_data = json_encode($data);
            $updated_by = $ci->session->userdata('id');
            $insert_data = [
                'table_name' => $table,
                'old_data' => json_encode($old_data),
                'new_data' => json_encode($data),
                'updated_by' => $ci->session->userdata('id'),
                'created_date' => date('Y-m-d H:i:s'),
            ];
            if ($ci->db->insert('system_logs',$insert_data)){
                return true;
            }
        }
        return false;
    }

}

function sendemail($params) {
    //Filtering @params
    $params['from_name'] = (!isset($params['from_name']) ) ? '' : $params['from_name'];
    $params['replytoname'] = (!isset($params['replytoname']) ) ? '' : $params['replytoname'];

    $ci = &get_instance();

    //Loading email library
    $ci->load->library('email');
    $mail = $ci->email;
    $mail->initialize(array('mailtype' => 'html'));

    //Set who the message is to be sent from
    $mail->from($params['from'], $params['from_name']);

    //Set an alternative reply-to address
    $mail->reply_to($params['replyto'], $params['replytoname']);

    //Set who the message is to be sent to
    $mail->to($params['sendto']);

    if (isset($params['bcc'])) {
        //Set the BCC address
        $mail->bcc($params['bcc']);
    }

    if (isset($params['cc'])) {
        //Set the CC address
        $mail->bcc($params['cc']);
    }

    //Set the subject line
    $mail->subject($params['subject']);

    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    $mail->message($params['message']);

    //send the message, check for errors
    if (!$mail->send()) {
        echo $mail->print_debugger();
        die;
    } else {
        return true;
    }
}

function getBrowser() {
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version = "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    } elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }

    // Next get the name of the useragent yes seperately and for good reason
    if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    } elseif (preg_match('/Firefox/i', $u_agent)) {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    } elseif (preg_match('/Chrome/i', $u_agent)) {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    } elseif (preg_match('/Safari/i', $u_agent)) {
        $bname = 'Apple Safari';
        $ub = "Safari";
    } elseif (preg_match('/Opera/i', $u_agent)) {
        $bname = 'Opera';
        $ub = "Opera";
    } elseif (preg_match('/Netscape/i', $u_agent)) {
        $bname = 'Netscape';
        $ub = "Netscape";
    }

    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }

    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
            $version = $matches['version'][0];
        } else {
            $version = $matches['version'][1];
        }
    } else {
        $version = $matches['version'][0];
    }

    // check if we have a number
    if ($version == null || $version == "") {
        $version = "?";
    }

    return array(
        'userAgent' => $u_agent,
        'name' => $bname,
        'version' => $version,
        'platform' => $platform,
        'pattern' => $pattern
    );
}

function segment($seg) {
    $ci = &get_instance();

    return $ci->uri->segment($seg);
}

function set_flash($index, $msg) {
    $ci = &get_instance();
    $ci->session->set_flashdata($index, $msg);
}

function swiftsend($param) {
    $ci = &get_instance();
    include_once APPPATH . '../vendor/autoload.php';

    $transport = new Swift_SmtpTransport($ci->config->item('smtp_host'), $ci->config->item('smtp_port'), $ci->config->item('smtp_encryption'));
    $transport->setUsername($ci->config->item('smtp_email_address'));
    $transport->setPassword($ci->config->item('smtp_password'));

    $mailer = new Swift_Mailer($transport);

    $message = new Swift_Message($param['subject']);
    $message->setFrom($param['from']);
    $message->setReplyTo($param['reply']);
    $message->setTo($param['to']);
    $message->setBody($param['msg'], 'text/html');
    $return = $mailer->send($message);

    return ( isset($param['return']) && $param['return'] ) ? $return : true;
}

function generate_thumb($params) {
    $crop = isset($params['crop']) ? $params['crop'] : 1;
    $path = $params['file'] != '' && file_exists($params['url'] . $params['file']) ? base_url(substr($params['url'], 2) . $params['file']) : base_url('images/no-image.jpg');

    return base_url() . 'assets/phpthumb/phpThumb.php?src=' . urlencode($path) . '&amp;w=' . $params['width'] . '&amp;h=' . $params['height'] . '&amp;zc=' . $crop;
}

function idToName($table, $id, $id_column = 'id', $value_column = 'name') {
    $ci = &get_instance();
    $data = $ci->db->select($value_column)->from($table)->where($id_column, $id)->get()->row();
    return $data ? $data->$value_column : '';
}

function hashPassword($password) {
    return md5($password);
}

function getDbDate($date) {
    return date('Y-m-d H:i:s', strtotime($date));
}

function getJobNumber($id, $return = false) {
    $job_number = 'LSF-J-' . ( 10000000000 + $id );
    if (!$return) {
        echo $job_number;
    } else {
        return $job_number;
    }
}

function getDateFormat($php = false) {
    return !$php ? 'MMM DD, YYYY' : 'm.d.Y';
}

if (!function_exists('name_email_format')) {

    function name_email_format($name, $email) {
        return $name . ' <' . $email . '>';
    }

}

if (!function_exists('getPriceFormat')) {

    function getPriceFormat($price, $currency = '€') {
        $ci = &get_instance();
        if (get_userdata('c_logged_in') && get_userdata('customer_id')) { // get currency from legal vehicle
            $currency_data = $ci->db->select('legal_vehicles.currency')
                            ->from('legal_vehicles')
                            ->join('legal_vehicle_branches', 'legal_vehicle_branches.legal_vehicle_id = legal_vehicles.id')
                            ->join('customers', 'customers.branch_id = legal_vehicle_branches.id')
                            ->where('customers.id', get_userdata('customer_id'))
                            ->get()->row();
            $currency = $currency_data ? $currency_data->currency : $currency;
        }

        return $currency . number_format($price);
    }

}

if (!function_exists('getCustomerMovementCount')) {

    function getCustomerMovementCount($customer_id = false) {
        $ci = &get_instance();
        $customer_id = !$customer_id ? get_userdata('customer_id') : $customer_id;
        $customer_movements = $ci->db->select('customer_storage.movements')
                        ->from('customer_storage')
                        ->where('customer_storage.customer_id', $customer_id)
                        ->get()->row()->movements;

        $customer_used_movements = $ci->db->select('count(lsf_jobs.id) as used_movements')
                        ->from('jobs')
                        ->where('jobs.customer_id', $customer_id)
                        ->where('jobs.first_job', 0)
                        ->where('jobs.status !=', 15)
                        //->where( 'jobs.status', 1 )
                        ->get()->row()->used_movements;

        return array('total_movements' => $customer_movements, 'used_movements' => $customer_used_movements);
    }

}

if (!function_exists('getStatus')) {

    function getStatus($status, $type = 'item') {
        if ($type == 'item') {
            switch ($status) {
                case 0:
                    return 'In storage';
                    break;
                case 1:
                    return 'Pending delivery';
                    break;
                case 2:
                    return 'Returned';
                    break;
                case 3:
                    return 'Not collected';
                    break;
                case 4:
                    return 'Collected';
                    break;
            }
        } elseif ('job') {
            switch ($status) {
                case 0:
                    return 'Created';
                    break;
                case 1:
                    return 'Confirmed';
                    break;
                case 2:
                    return 'Assigned';
                    break;
                case 3:
                    return 'Accepted';
                    break;
                case 4:
                    return 'Prepared';
                    break;
                case 5:
                    return 'Loaded in van';
                    break;
                case 6:
                    return 'Left for client';
                    break;
                case 7:
                    return 'Arrived at client';
                    break;
                case 8:
                    return 'Delivered';
                    break;
                case 9:
                    return 'Collected';
                    break;
                case 10:
                    return 'Departed from client';
                    break;
                case 11:
                    return 'Arrived at warehouse';
                    break;
                case 12:
                    return 'Loaded in storage space';
                    break;
                case 13:
                    return 'Rejected';
                    break;
                case 14:
                    return 'Prepare for delivery';
                    break;
                case 15:
                    return 'Cancel';
                    break;
            }
        }elseif('condition_report'){
            $condition_report = ['NON','New','Used','Scratched/damaged','PBO','Return from delivery'];
            return $condition_report[$status];
        }
    }

}

if (!function_exists('getItemTags')) {

    function getItemTags() {
        $ci = &get_instance();
        $return_tags = array();
        $customer_id = get_userdata('customer_id');
        $tags = $ci->db->select('customer_items.tags')
                        ->from('customer_items')
                        ->join('jobs', 'jobs.id = customer_items.job_id')
                        ->where('jobs.customer_id', $customer_id)
                        ->get()->result();

        foreach ($tags as $tag) {
            if (!$tag->tags) {
                continue;
            }
            $single_tag = explode(',', $tag->tags);
            foreach ($single_tag as $t) {
                if (!key_exists(strtolower($t), $return_tags)) {
                    $return_tags[strtolower($t)] = ucwords($t);
                }
            }
        }

        return $return_tags;
    }

}

if (!function_exists('getStatusList')) {

    function getStatusList($type) {
        $status = array();
        if ($type == 'item') {
            $status = array(
                '0' => 'In storage',
                '1' => 'Pending delivery',
                '2' => 'Returned',
                '3' => 'Not collected',
            );
        }

        return $status;
    }

}

if (!function_exists('getTagsView')) {

    function getTagsView($tags) {
        $tags = explode(',', $tags);
        foreach ($tags as $tag) {
            echo '<a href="#" class="tags" data-th="Tags">' . ucwords($tag) . '</a>';
        }
    }

}

if (!function_exists('isItemSelectedForCollection')) {

    function isItemSelectedForCollection($job_id, $item_id) {
        $ci = &get_instance();
        $data = $ci->db->get_where('job_item', array('item_id' => $item_id, 'job_id' => $job_id))->result();
        echo $data ? 'checked' : '';
    }

}

if (!function_exists('hideDetail')) {

    function hideDetail($data, $start = 0, $num = 3, $character = '*') {
        $data = str_split($data);
        for ($i = $start - 1; $i < $num + ( $start - 1 ); $i ++) {
            if ($data[$i] == ' ') {
                continue;
            } // skipping blank space
            $data[$i] = $character;
        }
        echo join('', $data);
    }

}

if (!function_exists('getTableRow')) {

    function getTableRow($table_name, $row_id) {
        $ci = &get_instance();
        $data = $ci->db->get_where($table_name, array('id' => $row_id))->row();
        if ($data)
            return $data;
        else
            return false;
    }

}

if (!function_exists('getNextPaymentDate')) {

    function getNextPaymentDate($customer_id = false) {
        $ci = &get_instance();
        $customer_id = $customer_id ? $customer_id : get_userdata('customer_id');
        $payment_date = date('Y-m-d', strtotime('first day of next month'));
        $next_payment_day_setting = $ci->db->select('customer_payments.*')
                        ->from('customer_payments')
                        ->where('customer_payments.customer_id', $customer_id)
                        ->get()->row();
        if ($next_payment_day_setting) {
            $next_payment_day_setting = ( $next_payment_day_setting->next_payment_date ) - 1;
            $payment_date = date(getDateFormat(true), strtotime($next_payment_day_setting . ' days', strtotime($payment_date)));
        }
        echo $payment_date;
    }

}

if (!function_exists('displayDate')) {

    function displayDate($date, $return = false) {
        $formated_date = date(getDateFormat(true), strtotime($date));
        if (!$return) {
            echo $formated_date;
        } else {
            return $formated_date;
        }
    }

}

if (!function_exists('getCustomerRemainingInsurance')) {

    function getCustomerRemainingInsurance($customer_id) {
        $ci = &get_instance();
        $allocated_insurance = 0;
        $data = $ci->db->select('SUM(lsf_customer_items.insurance_amount) as  allocated_insurance')
                        ->from('customer_items')
                        ->join('jobs', 'customer_items.job_id = jobs.id')
                        ->where('jobs.customer_id', $customer_id)
                        ->get()->row();
        if (isset($data->allocated_insurance) && $data->allocated_insurance) {
            $allocated_insurance = $data->allocated_insurance;
        }

        $total_insurance = $ci->db->select('(CASE WHEN insurance = 0 THEN "0" ELSE insurance_amount END) as total_insurance')
                        ->from('customer_storage')
                        ->where('customer_storage.customer_id', $customer_id)
                        ->get()->row()->total_insurance;

        return $total_insurance == 0 ? 0 : $total_insurance - $allocated_insurance;
    }

}

if (!function_exists('getCustomerVolume')) {

    function getCustomerVolume($customer_id) {
        $customer_used_volume = 0;
        $ci = &get_instance();

        $ci->db->select('storage_plans.title')
                ->from('customer_storage')
                ->join('storage_plans', 'storage_plans.id = customer_storage.storage_plan_id')
                ->where('customer_storage.customer_id', $customer_id);
        $customer_storage_plan = $ci->db->get()->row();
        $customer_storage_plan = ( is_numeric($customer_storage_plan->title) ) ? $customer_storage_plan->title : false;

        if ($customer_storage_plan) {
            $ci->db->select('cartons.volume')
                    ->from('customer_items')
                    ->join('cartons', 'cartons.id = customer_items.box_type')
                    ->where('customer_items.packing_type', 'boxed')
                    ->where('customer_items.id', $customer_id);
            $customer_boxed_items = $ci->db->get()->result();
            if ($customer_boxed_items) {
                foreach ($customer_boxed_items as $bitem) {
                    $customer_used_volume += (int) $bitem->volume;
                }
            }

            $ci->db->select('customer_items.box_type, customer_items.modified_dimention')
                    ->from('customer_items')
                    ->join('jobs', 'jobs.id = customer_items.job_id')
                    ->where('customer_items.packing_type', 'not_boxed')
                    ->where('jobs.customer_id', $customer_id);
            $customer_not_boxed_items = $ci->db->get()->result();
            if ($customer_not_boxed_items) {
                foreach ($customer_not_boxed_items as $nbitem) {
                    if ($nbitem->modified_dimention) {
                        $dimension = explode(',', $nbitem->modified_dimention);
                        $nb_volume = $dimension[0] * $dimension[1] * $dimension[2];
                    } else {
                        $dimension = explode(',', $nbitem->box_type);
                        $nb_volume = $dimension[0] * $dimension[1] * $dimension[2];
                    }
                    $customer_used_volume += (int) $nb_volume;
                }
            }
        }

        return array('customer_used_volume' => $customer_used_volume,
            'customer_storage_plan' => $customer_storage_plan
        );
    }

}

if (!function_exists('getInvoiceNumber')) {

    function getInvoiceNumber() {
        $ci = &get_instance();
        $invoice_number = $ci->db->select('invoices.invoice_number')
                        ->from('invoices')
                        ->order_by('id', 'desc')
                        ->where('invoices.status', 1)
                        ->get()->row();

        if ($invoice_number) {
            $invoice_number = (int) ltrim($invoice_number->invoice_number, 'I');
            $invoice_number += 1;
        } else {
            $invoice_number = '1000000001'; // first invoice number
        }

        return 'LSF-I-' . $invoice_number;
    }

}

if (!function_exists('getPrice')) {

    function getPrice($plan_type, $movement, $lv = false) {
        $ci = &get_instance();
        $ci->db->select("movement_{$movement}_price as price")
                ->from('storage_plans')
                ->where('title', $plan_type);

        if($lv) {
          $ci->db->where('legal_vehicle_id', $lv);
        }

        return $ci->db->get()->row()->price;
    }

}

if (!function_exists('getCustomerPendingTasks')) {
  function getCustomerPendingTasks($customer_id) {
    $ci = &get_instance();
    $ci->db->select('leads.*')
      ->from('leads')
      ->where('leads.customer_id', $customer_id);

    return $ci->db->get()->result();
  }
}