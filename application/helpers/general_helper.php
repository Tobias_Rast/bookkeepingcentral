<?php

if (!function_exists('loadLanguages')) {

    function loadLanguages() {
        $ci = & get_instance();

        if (isset($_SESSION['language'])) {
            $language = $_SESSION['language'];
            if (file_exists('application/language/' . $language . '/application_lang.php')) {
                $ci->lang->load('application_lang', $language);
            } else {
                $ci->lang->load('application_lang');
            }
        } else {
            $ci->lang->load('application_lang');
        }
    }

}

if (!function_exists('fetchLine')) {

    function fetchLine($line) {

        return $line; // Uncomment use language module
        $ci = & get_instance();

        $lowerline = strtolower(str_replace(' ', '_', $line));

        return ($ci->lang->line($lowerline) != '' ? $ci->lang->line($lowerline) : $line);
    }

}

if (!function_exists('getLanguage')) {

    function getLanguage() {
        $ci = & get_instance();
        $result = $ci->db->get('languages')->result_array();
        return $result;
    }

}

// Generate hierarchy
if (!function_exists('buildTree')) {

    function buildTree(array $elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            $element = (array) $element;
            if ($element['parent_id'] == $parentId) {
                $children = buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

}

/**
 * Get all the modules in system */
if (!function_exists('getModules')) {

    function getModules() {
        $return = [];
        $dirs = glob('application/modules/*', GLOB_ONLYDIR);
        foreach ($dirs as $dir) {
            $splited = explode('/', $dir);
            $return[] = end($splited);
        }
        return $return;
    }

}

/**
 * Get all the modules in system */
if (!function_exists('getClasses')) {

    function getClasses($module) {
        $return = [];
        $controllers = glob('application/modules/' . $module . "/controllers/*.php");
        foreach ($controllers as $controller) {
            $return[] = basename($controller, ".php");
        }
        return $return;
    }

}
/**
 * Get all the methods in system */
if (!function_exists('getMethods')) {

    function getMethods($module, $controller) {

        $ci = & get_instance();
        $controller_path = "application/modules/$module/controllers/$controller.php";
        if ($controller != 'Permissions') {
            $ci->load->file($controller_path);
        }
        $methods = get_class_methods($controller);
        foreach ($methods as $method) {
            $reflect = new ReflectionMethod($controller, $method);
            if ($reflect->isPublic() && $reflect->class == $controller && $method != '__construct') {
                $return[] = $method;
            }
        }
        return $return;
    }

}

/*
 * Update version of content
 *  */

if (!function_exists('updateVersion')) {

    function updateVersion($key) {
        $ci = & get_instance();
        $ci->db->set('value', 'value+1', FALSE);
        $ci->db->where('key', $key);
        $ci->db->update('versions');
    }

}

/*
 * Update version of crew user
 *  */

if (!function_exists('updateUserVersion')) {

    function updateUserVersion($user_id) {
        $ci = & get_instance();
        $ci->db->set('version', 'version+1', FALSE);
        $ci->db->where('id', $user_id);
        $ci->db->update('users');
    }

}

/*
 * Funciton to check if superuser
 *  */
if (!function_exists('isSuperuser')) {

    function isSuperuser() {
        $ci = & get_instance();
        $superuser_roles = [1];
        if (in_array($ci->session->userdata('role_id'), $superuser_roles)) {
            return true;
        }
        return false;
    }

}

/*
 * Funciton to strip tags of validation messages 
 *  */
if (!function_exists('stripErrors')) {

    function stripErrors($errors) {
        return (string) str_replace(array("\r\n", "\n", "\r"), ' ', (strip_tags($errors)));
    }

}

/*
 * Forgot password email
 *  */

if (!function_exists('sendForgotPasswordEmail')) {

    function sendForgotPasswordEmail($email, $reset_token) {
        $ci = & get_instance();

        $to = $email;
        $subject = 'Reset Password';
        $data['link'] = base_url() . 'auth/resetPassword?email=' . $email . '&reset_token=' . $reset_token;
        $message = $ci->load->view('auth/mail_template', $data, true);

        $headers = 'From: info@mbca.pro' . "\r\n" .
                'MIME-Version: 1.0' . "\r\n" .
                'Content-type: text/html; charset=utf-8';

        if (@mail($to, $subject, $message, $headers)) {
            return true;
        }
    }

}

if (!function_exists('isJson')) {

    function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

}

if (!function_exists('get_userdata')) {

    function get_userdata($index) {
        $ci = &get_instance();

        return $ci->session->userdata($index) ? $ci->session->userdata($index) : false;
    }

}

if (!function_exists('debug')) {

    function debug($data, $die = true) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        if ($die) {
            die;
        }
    }

}

if (!function_exists('hashPassword')) {

    function hashPassword($password) {
        return md5($password);
    }

}
