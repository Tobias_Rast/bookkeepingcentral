<?php

class Base_Controller extends MX_Controller {

    public $customers_modal;

    public function __construct() {
        parent::__construct();
        $exclude = array('setLanguage', 'language_file_android');
        if (!in_array($this->router->method, $exclude) && !$this->session->userdata('logged_in')) {
            redirect('auth');
        }
        if (!in_array($this->router->method, $exclude) && (!$this->acl->has_permission($this->router->fetch_module() . '-' . strtolower($this->router->class) . '-' . $this->router->method))) {
            redirect('login/forbidden');
        }
        //loadLanguages();
    }

}
