<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class MY_Model extends CI_Model {

    protected $table;

    public function __construct() {
        parent::__construct();
    }

    public function create($myData) {
        if ($this->db->field_exists('created_date', $this->table))
            $myData['created_date'] = getDbDate(date('Y-m-d H:i:s'));
        if ($this->db->field_exists('updated_date', $this->table))
            $myData['updated_date'] = getDbDate(date('Y-m-d H:i:s'));
        if ($this->db->field_exists('created_by', $this->table) && (get_userdata('id') || get_userdata('cid')))
            $myData['created_by'] = get_userdata('id') ? get_userdata('id') : get_userdata('cid');
        if ($this->db->field_exists('updated_by', $this->table) && (get_userdata('id') || get_userdata('cid')))
            $myData['updated_by'] = get_userdata('id') ? get_userdata('id') : get_userdata('cid');

        $this->db->insert($this->table, $myData);

        return $this->db->insert_id();
    }

    public function update($condition, $myData) {
        if ($this->db->field_exists('updated_date', $this->table))
            $myData['updated_date'] = getDbDate(date('Y-m-d H:i:s'));
        if ($this->db->field_exists('updated_by', $this->table))
            $myData['updated_by'] = get_userdata('id') ? get_userdata('id') : get_userdata('cid');

        $this->db->where($condition);

        return $this->db->update($this->table, $myData);
    }

    public function delete($myId) {
        $this->db->where('id', $myId);

        return $this->db->delete($this->table);
    }

    public function deleteCondition($condition) {
        $this->db->where($condition);

        return $this->db->delete($this->table);
    }

    public function find($field_value, $field_name = 'id') {
        return $this->db->select('*')
                        ->from($this->table)
                        ->where($field_name, $field_value)
                        ->get()
                        ->row();
    }

    public function first() {
        return $this->db->select('*')
                        ->from($this->table)
                        ->get()
                        ->first_row();
    }

    public function all($array = null, $order = null) {
        $result = $this->db->from($this->table);

        if ($order) {
            $this->db->order_by($order);
        }

        if ($array) {
            $result = $result->get()->result_array();
        } else {
            $result = $result->get()->result();
        }

        return $result;
    }

    public function getWhere($condition) {
        $this->db->from($this->table);

        if ($condition) {
            $this->db->where($condition);
        }

        return $this->db->get()->result();
    }

    public function get_single($condition) {
        $this->db->from($this->table);

        if ($condition) {
            $this->db->where($condition);
        }

        return $this->db->get()->row();
    }

    public function increaseVersion() {
        $this->db->where('key', 'data_version');
        $this->db->set('value', 'value+1', FALSE);
        $this->db->update('versions');
    }

}
