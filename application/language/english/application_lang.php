<?php 
$lang["accounts"] = "Accounts"; 
$lang["cancel_collection"] = "Cancel collection"; 
$lang["collection_schedule"] = "Collection schedule"; 
$lang["customer"] = "Customer"; 
$lang["customer_registration"] = "Customer Registration"; 
$lang["dashboard"] = "Dashboard"; 
$lang["delivery_schedule"] = "Delivery schedule"; 
$lang["email_address"] = "Email address"; 
$lang["enter_your_password"] = "Enter your password"; 
$lang["fixed_storage"] = "Fixed Storage"; 
$lang["forgot_password"] = "Forgot password"; 
$lang["having_problems?_live_chat_with_us"] = "Having problems? Live chat with us"; 
$lang["indicates_a_required_field"] = "indicates a required field"; 
$lang["live_chat"] = "Live Chat"; 
$lang["login"] = "Login"; 
$lang["manage_account"] = "Manage account"; 
$lang["member_login"] = "Member Login"; 
$lang["movements"] = "Movements"; 
$lang["next_direct_debit_fee"] = "Next direct debit fee"; 
$lang["next_payment_date"] = "Next payment date"; 
$lang["notices"] = "Notices"; 
$lang["no_delivery_scheduled_(empty_case)"] = "No delivery scheduled (empty case)"; 
$lang["ot_a_member?_sign-up_here"] = "ot a member? Sign-up here"; 
$lang["password"] = "Password"; 
$lang["payments"] = "Payments"; 
$lang["recent_notices"] = "Recent notices"; 
$lang["request_for_quote"] = "Request for Quote"; 
$lang["sign_out"] = "Sign Out"; 
$lang["storage"] = "Storage"; 
$lang["storage_space_usage"] = "Storage space usage"; 
$lang["update_detail"] = "Update detail"; 
$lang["update_details"] = "Update details"; 
$lang["you_are_currently_paying_for_unused_space,_consider_upgrading_to_our_variable_storage_plan_to_save_money."] = "You are currently paying for unused space, consider upgrading to our variable storage plan to save money."; 
$lang["you_are_only_using_80%_of_your_storage_space"] = "You are only using 80% of your storage space"; 
?>