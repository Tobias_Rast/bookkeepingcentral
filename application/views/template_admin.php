<?php $this->load->view( 'inc/template_head.php' ); ?>
<body>
<!-- [ Pre-loader ] start -->
<?php if ( $_SERVER['CI_ENV'] != 'development' ) { ?>
    <div class="loader-bg">
        <div class="loader-bar"></div>
    </div>
<?php } ?>
<div class="ajax-modal-container">
    <div class="modal fade" id="modal-form" role="dialog"></div>
</div>
<!-- [ Pre-loader ] end -->

<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
        <!-- [ Header ] start -->
		<?php $this->load->view( 'inc/header' ) ?>
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
				<?php $this->load->view( 'inc/navigation' ) ?>

                <div class="pcoded-content">
					<?php $this->load->view( $page, @$data ) ?>
                </div>
            </div>
        </div>
        <!-- [ Header ] end -->
    </div>
</div>

<?php $this->load->view( 'inc/footer_script.php' ); ?>
<div class="modal fade" id="select-customer-modal" role="dialog" aria-labelledby="select-customerModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="select-customerModalLabel"><?= fetchLine( 'Select customer first' ) ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <select id="select-customer-menu" class="form-control select2">
                    <option value=""><?= fetchLine( 'Select customer' ) ?></option>
					<?php foreach ( $this->customers_modal as $customer ) { ?>
                        <option value="<?= $customer->id ?>"><?= idToName( 'users', $customer->user_id ) ?></option>
					<?php } ?>
                </select>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="job_detail_modal" role="dialog" aria-labelledby="job_detail_modalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 960px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="job_detail_modalLabel"><?= fetchLine( 'Job detail' ) ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="generic-job-detail" class="modal-body">
            </div>
        </div>
    </div>
</div>
<script>
    var baseUrl = "<?= base_url();?>";
</script>
<script>
    (function ($) {
        $(document).ready(function () {

            $('.select2').select2();

            $('body').on('focus', "#preferred-timepicker", function () {
                $('#preferred-timepicker').daterangepicker({
                    timePicker: true,
                    timePicker24Hour: true,
                    timePickerIncrement: 15,
                    locale: {
                        format: 'HH:mm'
                    }
                }).on('show.daterangepicker', function (ev, picker) {
                    picker.container.find(".calendar-table").hide();
                });
            });

        });
    })(jQuery);
</script>
</body>

</html>
