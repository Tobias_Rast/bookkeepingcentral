<nav class="navbar header-navbar pcoded-header">
  <div class="navbar-wrapper">
    <div class="navbar-logo">
      <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
        <i class="feather icon-toggle-right"></i>
      </a>
      <a href="<?= base_url('dashboard') ?>" style="margin:0;">
        <!--<img class="img-fluid" src="<?= base_url('files/assets/images/logo.png') ?>" alt="<?= SITE_NAME ?>" style="width:40px;"/>-->
        <?= SITE_NAME ?>
      </a>
      <a class="mobile-options waves-effect waves-light">
        <i class="feather icon-more-horizontal"></i>
      </a>
    </div>
    <div class="navbar-container container-fluid">
      <ul class="nav-right">
            <li class="user-profile header-notification">
              <div class="dropdown-primary dropdown">
                <div class="dropdown-toggle" data-toggle="dropdown">
                  <span><?= get_userdata('name') ?></span>
                  <i class="feather icon-chevron-down"></i>
                </div>
                <ul class="show-notification profile-notification dropdown-menu"
                data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                <li>
                  <a href="auth/profile">
                    <i class="feather icon-user"></i> <?php echo fetchLine( 'Profile' ); ?>
                  </a>
                </li>
                <li>
                  <a href="<?= base_url('auth/logout') ?>">
                    <i class="feather icon-log-out"></i> <?php echo fetchLine( 'Logout' ); ?>
                  </a>
                </li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
