<?php
$primary_nav = $this->config->item('primary_nav');
$template = $this->config->item('template');
?>
<!-- [ navigation menu ] start -->
<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar main-menu">
        <div class="">
            <div class="main-menu-header">
                <?php $avatar_url = get_userdata('avatar') ? "uploads/users/" . get_userdata('avatar') : "files/assets/images/avatar-blank.jpg"; ?>
                <img class="img-menu-user img-radius" src="<?= $avatar_url ?>" alt="User-Profile-Image">
                <div class="user-details">
                    <p id="more-details"><?= get_userdata('name') ?><i class="feather m-l-10"></i></p>
                </div>
            </div>
        </div>
        <ul class="pcoded-item pcoded-left-item">
            <?php
            if ($primary_nav) {
                foreach ($primary_nav as $key => $link) {

                    // patch for sub nav customers
                    if ($link['permission'] == 'lsf_customers-lsf_customers-index') {
                        $link['sub'] = $customer_menu;
                    }
                    // patch for sub nav customers


                    $permission_exploded = explode('-', @$link['permission']);
                    if (count($permission_exploded) == 2) {
                        $permission = $permission_exploded[0] . '-' . $permission_exploded[0] . '-' . $permission_exploded[1];
                    } else {
                        $permission = @$link['permission'];
                    }
                    if (!$this->acl->has_permission($permission)) {
                        continue;
                    }

                    $link_class = '';
                    $li_active = '';
                    $menu_link = '';
                    $has_sub = false;

                    // Get 1st level link's vital info
                    $url = (isset($link['url']) && $link['url']) ? $link['url'] : '#!';
                    $active = (isset($link['url']) && (($template['active_module'] == $link['url']) || ($template['active_module'] == 'lsf_' . $link['url']))) ? ' active ' : '';
                    $icon = (isset($link['icon']) && $link['icon']) ? '<i class="' . $link['icon'] . '"></i>' : '';
                    $has_sub = isset($link['sub']);


                    if ($template['active_page'] == 'permissions') {
                        $active = (isset($link['url']) && ($link['url'] . '/' . $template['active_page'] == 'users/permissions')) ? ' active ' : '';
                    }
                    if ($template['active_page'] == 'roles') {
                        $active = (isset($link['url']) && ($link['url'] . '/' . $template['active_page'] == 'users/roles')) ? ' active ' : '';
                    }
                    ?>
                    <li class="<?= ($has_sub) ? 'pcoded-hasmenu' : ''; ?> <?= $active ?> <?= ($has_sub && $active) ? 'pcoded-trigger' : ''; ?>">
                        <a href="<?= ($has_sub) ? 'javascript:void(0);' : $url; ?>" class="waves-effect waves-dark <?= isset($link['tempclass']) ? $link['tempclass'] : '' ?>">
                            <span class="pcoded-micon"><?= $icon ?></span>
                            <span class="pcoded-mtext"><?= fetchLine($link['name']) ?></span>
                        </a>
                        <?php
                        if ($has_sub) {
                            foreach ($link['sub'] as $sub) {
                                $splits = explode('-', $sub['permission']);
                                if (count($splits) == 2) {
                                    $sub_permission = $splits[0] . '-' . $splits[0] . '-' . $splits[1];
                                } else {
                                    $sub_permission = @$sub['permission'];
                                }
                                if (!$this->acl->has_permission($sub_permission)) {
                                    continue;
                                }
                                ?>
                                <ul class="pcoded-submenu">
                                    <?php
                                    $sub_active = ($template['active_page'] == $splits[1] && $template['active_method'] == $splits[2]) ? 'active' : '';

                                    if ($template['active_page'] == 'permissions') {
                                        $sub_active = ($template['active_page'] == $splits[1] && $template['active_method'] == $splits[2]) ? 'active' : '';
                                    }
                                    if ($template['active_page'] == 'roles') {
                                        $sub_active = ($template['active_page'] == $splits[1] && $template['active_method'] == $splits[2]) ? 'active' : '';
                                    }
                                    ?>
                                    <li class="<?= $sub_active ?>">
                                        <a <?= isset($sub['data_field']) ? 'data-menu_type=' . $sub['data_field'] : '' ?> href="<?= $sub['url'] ?>" id="<?= isset($sub['id']) ? $sub['id'] : '' ?>" class="waves-effect waves-dark <?= isset($sub['tempclass']) ? $sub['tempclass'] : '' ?>">
                                            <span class="pcoded-mtext"><?= fetchLine($sub['name']) ?></span>
                                        </a>
                                    </li>
                                </ul>
                                <?php
                            }
                        }
                        ?>
                    </li>

                    <?php
                }
            }
            ?>
        </ul>
    </div>
</nav>
<!-- [ navigation menu ] end -->