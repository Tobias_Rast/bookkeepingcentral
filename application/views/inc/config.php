<?php

/**
 * config.php
 *
 * Author: phoenixcoded
 *
 * Configuration file. It contains variables used in the template as well as the primary navigation array from which the navigation is created
 *
 */
/* Template variables */
$class = $this->router->class;
$method = $this->router->method;

$template = array(
    'title' => SITE_NAME,
    'author' => 'Pegotec Pte. Ltd.',
    'description' => 'Control panel for ' . SITE_NAME,
    'keywords' => '',
    'active_page' => $class,
    'active_method' => $method
);

$primary_nav = array(
    array(
        'name' => 'Dashboard',
        'icon' => 'fas fa-dashboard',
        'url' => 'dashboard',
        'permission' => 'dashboard-index',
    ),
    array(
        'name' => 'Messages',
        'icon' => 'fa fa-ticket',
        'url' => 'tickets',
        'permission' => 'tickets-index',
        'sub' => array(
            array('name' => 'Messages',
                'icon' => 'fa fa-ticket',
                'url' => 'tickets',
                'permission' => 'tickets-index'
            ),
            array('name' => 'Resolved Messages',
                'icon' => 'fa fa-ticket',
                'url' => 'tickets/resolved',
                'permission' => 'tickets-resolved'
            )
        )
    ),
    array(
        'name' => 'Languages',
        'icon' => 'fas fa-language',
        'url' => 'languages',
        'permission' => 'languages-index',
    ),
    array(
        'name' => 'Departments',
        'icon' => 'fa fa-university',
        'url' => 'departments',
        'permission' => 'departments-index'
    ),
    array(
        'name' => 'Staff Members',
        'icon' => 'fa fa-support',
        'url' => 'support',
        'permission' => 'support-index'
    ),
    array(
        'name' => 'Crew Members',
        'icon' => 'fa fa-group',
        'url' => 'crew',
        'permission' => 'crew-index'
    ),
    array(
        'name' => 'User Settings',
        'icon' => 'fa fa-user',
        'url' => 'users',
        'permission' => 'users-index',
        'sub' => array(
            array('name' => 'Users',
                'icon' => 'fa fa-user',
                'url' => 'users',
                'permission' => 'users-index'
            ),
            array('name' => 'Permissions',
                'icon' => 'fa fa-dashboard',
                'url' => 'users/permissions',
                'permission' => 'users-permissions-index'
            ),
            array('name' => 'Roles',
                'icon' => 'fa fa-dashboard',
                'url' => 'users/roles',
                'permission' => 'users-roles-index'
            )
        )
    ),
    array(
        'name' => 'Console',
        'icon' => 'fa fa-puzzle-piece',
        'url' => 'api/console',
        'permission' => 'api-Console-index'
    ),
    array(
        'name' => 'Language',
        'icon' => 'fa fa-language',
        'url' => 'languages',
        'permission' => 'languages-index'
    )
);
?>