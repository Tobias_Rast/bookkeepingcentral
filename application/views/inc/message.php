<?php if($this->session->flashdata('success') != ''){ ?>
<div class="alert alert-success"><?=$this->session->flashdata('success');?></div>
<?php } ?>
<?php if($this->session->flashdata('info') != ''){ ?>
<div class="alert alert-info"><?=$this->session->flashdata('info');?></div>
<?php } ?>
<?php if($this->session->flashdata('warning') != ''){ ?>
<div class="alert alert-warning"><?=$this->session->flashdata('warning');?></div>
<?php } ?>
<?php if($this->session->flashdata('danger') != ''){ ?>
<div class="alert alert-danger"><?=$this->session->flashdata('danger');?></div>
<?php } ?>