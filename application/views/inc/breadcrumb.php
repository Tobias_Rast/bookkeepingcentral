<div class="page-header">
	<div class="page-block">
		<div class="row align-items-center">
			<div class="col-sm-8">
				<div class="page-header-title">
					<h4 class="m-b-10"><?= $crumbs[0]['page_title'] ?></h4>
				</div>
				<ul class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="dashboard">
							<i class="feather icon-home"></i>
						</a>
					</li>
					<?php foreach($crumbs as $k=>$crumb){ 
                                            if($k == 0) continue; 
                                            ?>
					<li class="breadcrumb-item">
						<a href="<?= $crumb['url'] ?>"><?= $crumb['page_title'] ?></a>
					</li>
                                        <?php } ?>
				</ul>
			</div>
		</div>
	</div>
</div>