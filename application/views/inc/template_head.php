<!DOCTYPE html>
<html lang="en">

<head>
  <title><?php echo SITE_NAME ?></title>
  <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 10]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Meta -->
  <!-- Meta -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="description" content="<?php echo $this->config->item('description', 'template') ?>">
  <meta name="keywords" content="<?php echo $this->config->item('keywords', 'template') ?>">
  <meta name="author" content="<?php echo $this->config->item('author', 'template') ?>">
  <!-- Favicon icon -->
  <base href="<?= site_url() ?>" id="asddssa">

  <!-- Favicon icon -->
  <link rel="icon" href="<?= base_url() ?>files/assets/images/favicon.ico" type="image/x-icon">
  <!-- Google font-->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
  <!-- Required Fremwork -->
  <!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>files/bower_components/bootstrap/css/bootstrap.min.css"> -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>files/assets/icon/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>files/assets/icon/icofont/css/icofont.css">
    <link rel="stylesheet" href="files/assets/datepicker/css/daterangepicker.css" media="screen, print">
    <!-- waves.css -->
  <link rel="stylesheet" href="<?= base_url() ?>files/assets/pages/waves/css/waves.min.css" type="text/css" media="all">
  <!-- feather icon -->
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>files/assets/icon/feather/css/feather.css">
  <!-- Style.css -->
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>files/assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>files/assets/css/pages.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>files/assets/css/widget.css">
  <!-- Data Table Css -->
  <link rel="stylesheet" type="text/css"
  href="<?= base_url() ?>files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>files/assets/pages/data-table/css/buttons.dataTables.min.css">
  <link rel="stylesheet" type="text/css"
  href="<?= base_url() ?>files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">

  <!-- Select 2 css -->
  <link rel="stylesheet" href="files/bower_components/select2/css/select2.min.css" />
  <!-- Multi Select css -->
  <link rel="stylesheet" type="text/css" href="files/bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css" />
  <link rel="stylesheet" type="text/css" href="files/bower_components/multiselect/css/multi-select.css" />
  <!-- Style.css -->


  <!-- Switch component css -->
  <link rel="stylesheet" type="text/css" href="files/bower_components/switchery/css/switchery.min.css">


  <link rel="stylesheet" type="text/css" href="files/assets/pages/timeline/style.css">
  
  <!-- flag icon -->
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>files/assets/icon/flag-icons/css/flag-icon.css">
  
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>files/assets/jquery-fullcalendar/fullcalendar.min.css">

  <!-- Font Awesome 5 -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

  <!-- Main Css -->
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>files/assets/css/main.css">

  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>files/bower_components/jquery-ui/css/jquery-ui.css">

  <script type="text/javascript" src="<?= base_url() ?>files/bower_components/jquery/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
</head>
<?php         loadLanguages(); ?>
