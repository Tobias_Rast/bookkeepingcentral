<script type="text/javascript" src="<?= base_url() ?>files/bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>files/bower_components/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>files/assets/jquery-validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>files/assets/datepicker/js/moment.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>files/assets/datepicker/js/daterangepicker.js"></script>
<!-- waves js -->
<script src="<?= base_url() ?>files/assets/pages/waves/js/waves.min.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?= base_url() ?>files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- data-table js -->
<script src="<?= base_url() ?>files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>files/assets/pages/data-table/js/jszip.min.js"></script>
<script src="<?= base_url() ?>files/assets/pages/data-table/js/pdfmake.min.js"></script>
<script src="<?= base_url() ?>files/assets/pages/data-table/js/vfs_fonts.js"></script>
<script src="<?= base_url() ?>files/assets/js/repeater.js"></script>
<script src="<?= base_url() ?>files/assets/jquery-fullcalendar/fullcalendar.min.js"></script>
<script src="<?= base_url() ?>files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?= base_url() ?>files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url() ?>files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url() ?>files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

<!-- Google map js -->
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script type="text/javascript" src="<?= base_url() ?>files/assets/pages/google-maps/gmaps.js"></script>

<!-- Select 2 js -->
<script type="text/javascript" src="files/bower_components/select2/js/select2.full.min.js"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="files/bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js">
</script>
<script type="text/javascript" src="files/bower_components/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="files/assets/js/jquery.quicksearch.js"></script>

<!-- modernizr js -->
<script type="text/javascript" src="files/bower_components/modernizr/js/modernizr.js"></script>
<script type="text/javascript" src="files/bower_components/modernizr/js/css-scrollbars.js"></script>
<!-- Switch component js -->
<script type="text/javascript" src="files/bower_components/switchery/js/switchery.min.js"></script>
<!-- Tags js -->
<script type="text/javascript" src="files/bower_components/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>


<!-- Custom js -->
<!--<script type="text/javascript" src="files/assets/pages/advance-elements/swithces.js"></script>-->
<script type="text/javascript" src="files/assets/pages/advance-elements/select2-custom.js"></script>
<script src="<?= base_url() ?>files/assets/js/pcoded.min.js"></script>
<script src="<?= base_url() ?>files/assets/js/vertical/vertical-layout.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>files/assets/js/script.min.js"></script>

<script src="files/assets/pages/data-table/js/data-table-custom.js"></script>
<!-- notification js -->
<script type="text/javascript" src="files/assets/js/bootstrap-growl.min.js"></script>
<script type="text/javascript" src="files/assets/pages/notification/notification.js"></script>

<script type="text/javascript">
    (function ($) {
        $('#job-calendar').fullCalendar({
            height: 'auto',
            defaultView: 'basicWeek',
            events: 'lsf_operations/jobJson',
            eventRender: function (event, eventElement) {
                eventElement.find('.fc-title').html(event.title);
                if (event.status == 2) {
                    eventElement.addClass('green-row');
                } else if (event.status == 1) {
                    eventElement.addClass('orange-row');
                } else if (event.status == 0) {
                    eventElement.addClass('red-row');
                }
            }
        });
    })(jQuery)
</script>

<script>
    $(document).ready(function () {

        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

        $(document).ready(function () {
            $('[data-toggle="popover"]').popover({
                html: true,
                content: function () {
                    return $('#primary-popover-content').html();
                }
            });
        });


        $('.form-control').addClass('fill');


        function notify(message, type) {
            $.growl({
                message: message
            }, {
                type: type,
                allow_dismiss: true,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 10500,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }
        ;
<?php if ($this->session->flashdata('success') != '') { ?>
            notify('<?= $this->session->flashdata('success'); ?>', 'success');
<?php } ?>
<?php if ($this->session->flashdata('info') != '') { ?>
            notify('<?= $this->session->flashdata('info'); ?>', 'info');
<?php } ?>
<?php if ($this->session->flashdata('warning') != '') { ?>
            notify('<?= $this->session->flashdata('warning'); ?>', 'warning');
<?php } ?>
<?php if ($this->session->flashdata('danger') != '') { ?>
            notify('<?= $this->session->flashdata("danger"); ?>', 'danger');
<?php } ?>

    })
</script>