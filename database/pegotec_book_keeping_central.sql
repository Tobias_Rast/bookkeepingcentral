/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 10.1.25-MariaDB : Database - pegotec_book_keeping_central
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `bkc_permissions` */

DROP TABLE IF EXISTS `bkc_permissions`;

CREATE TABLE `bkc_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;

/*Data for the table `bkc_permissions` */

/*Table structure for table `bkc_role_permissions` */

DROP TABLE IF EXISTS `bkc_role_permissions`;

CREATE TABLE `bkc_role_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `permission_id` (`permission_id`),
  CONSTRAINT `bkc_role_permissions_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `bkc_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `bkc_role_permissions_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `bkc_permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=653 DEFAULT CHARSET=utf8;

/*Data for the table `bkc_role_permissions` */

/*Table structure for table `bkc_roles` */

DROP TABLE IF EXISTS `bkc_roles`;

CREATE TABLE `bkc_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `bkc_roles` */

insert  into `bkc_roles`(`id`,`name`,`description`) values (1,'Superadmin','Administration');

/*Table structure for table `bkc_settings` */

DROP TABLE IF EXISTS `bkc_settings`;

CREATE TABLE `bkc_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` text,
  `value` text,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `bkc_settings` */

/*Table structure for table `bkc_users` */

DROP TABLE IF EXISTS `bkc_users`;

CREATE TABLE `bkc_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` text,
  `password` text,
  `salt` text,
  `token` text,
  `push_notification_token` text,
  `device_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=Android,2=iOS,0=Not Defined',
  `verification_code` text,
  `last_login` datetime DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '0=inactive,1=active,2=email verified',
  `status_changed_date` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lsf user role` (`role_id`),
  CONSTRAINT `lsf user role` FOREIGN KEY (`role_id`) REFERENCES `bkc_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

/*Data for the table `bkc_users` */

insert  into `bkc_users`(`id`,`role_id`,`name`,`username`,`email`,`password`,`salt`,`token`,`push_notification_token`,`device_type`,`verification_code`,`last_login`,`avatar`,`status`,`status_changed_date`,`created_date`,`updated_date`) values (1,1,'Superadmin','superadmin','sujeet@pegotec.net','21232F297A57A5A743894A0E4A801FC3',NULL,'3368fdb4e9846edb88b9c54e4d64efb3','',1,NULL,'2019-01-15 08:43:46','',1,NULL,'2018-10-25 17:20:20','2018-10-25 17:20:23');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
