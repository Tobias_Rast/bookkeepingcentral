# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.23)
# Database: pegotec_book_keeping_central
# Generation Time: 2019-03-17 11:17:31 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table bkc_earning_rates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bkc_earning_rates`;

CREATE TABLE `bkc_earning_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `account_code` varchar(255) DEFAULT NULL,
  `type_of_units` varchar(255) DEFAULT NULL,
  `is_exempt_from_tax` varchar(255) DEFAULT NULL,
  `is_exempt_from_super` varchar(255) DEFAULT NULL,
  `earnings_type` varchar(255) DEFAULT NULL,
  `earnings_rate_id` varchar(255) DEFAULT NULL,
  `rate_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `bkc_earning_rates` WRITE;
/*!40000 ALTER TABLE `bkc_earning_rates` DISABLE KEYS */;

INSERT INTO `bkc_earning_rates` (`id`, `name`, `account_code`, `type_of_units`, `is_exempt_from_tax`, `is_exempt_from_super`, `earnings_type`, `earnings_rate_id`, `rate_type`)
VALUES
	(1,'100 - Hours Worked','477','Hours','false','false','ORDINARYTIMEEARNINGS','d171742d-192c-448c-9e99-edfd1a80a689','RATEPERUNIT'),
	(2,'Overtime Hours (exempt from super)','477','Hours','false','true','OVERTIMEEARNINGS','ecf74713-9366-4010-859d-daf982c9a853','RATEPERUNIT'),
	(3,'Redundancy','477',NULL,'true','true','LUMPSUMD','da853de2-e29d-4fe7-8a58-a4c02225d7f5','FIXEDAMOUNT'),
	(4,'ETP Leave Earning','477','Hours','false','true','EMPLOYMENTTERMINATIONPAYMENT','358a7192-2ab7-4123-b2ec-bed4043fc910','RATEPERUNIT'),
	(5,'100 - Hours Worked - Adjustment','477','Hours','false','false','ORDINARYTIMEEARNINGS','75dc99f6-04b3-44b5-bed7-2ee9068bbda8','RATEPERUNIT'),
	(6,'100 - Hours Worked (OT 1.5)','477','Hours','false','true','OVERTIMEEARNINGS','942079d9-67a0-498a-bd1a-efbad5fd5434','RATEPERUNIT'),
	(7,'100 - Hours Worked (x1.5)','477','Hours','false','true','OVERTIMEEARNINGS','98de56ed-c4d4-43be-88a1-a67c41bdf956','RATEPERUNIT'),
	(8,'100 - Hours Worked (OT 2)','477','Hours','false','true','OVERTIMEEARNINGS','1ca1c1ed-487a-4ce7-b144-8ae431a7354c','RATEPERUNIT'),
	(9,'110 - Public Holiday Not Worked','477','Hours','false','false','ORDINARYTIMEEARNINGS','210b615e-a5b5-408e-ac7c-d615bfd8a374','RATEPERUNIT'),
	(10,'120 - Public Holiday Worked','477','Hours','false','false','ORDINARYTIMEEARNINGS','16496f02-fa50-4d25-adde-4be7b3e8f59b','RATEPERUNIT'),
	(11,'120 - Public Holiday Worked - Adjustment','477','Hours','false','false','ORDINARYTIMEEARNINGS','a5e339e5-ff50-461c-ab93-b24e79ec0134','RATEPERUNIT'),
	(12,'155 - Inclement Weather (Unpaid)','477','Hours','false','false','ORDINARYTIMEEARNINGS','e0364ac1-c668-4c42-9b18-8533b9484c73','RATEPERUNIT'),
	(13,'156 - Inclement Weather (Paid)','477','Hours','false','false','ORDINARYTIMEEARNINGS','86798248-dc75-4adf-ba93-efaa0a79f782','RATEPERUNIT'),
	(14,'156 - Inclement Weather (Paid) - Adjustment','477','Hours','false','false','ORDINARYTIMEEARNINGS','ecc17c40-2d7f-4825-b891-c4b23e924217','RATEPERUNIT'),
	(15,'156 - Inclement Weather (Paid) x1.5','477','Hours','false','true','OVERTIMEEARNINGS','f4ab80de-3cf8-4835-88db-f4d5d83d9b3b','RATEPERUNIT'),
	(16,'156 - Inclement Weather (Paid) x2','477','Hours','false','true','OVERTIMEEARNINGS','1453ea96-b322-4d20-a312-526d51f7b6ff','RATEPERUNIT'),
	(17,'156 - Inclement Weather (Paid) x2 - Adjustment','477','Hours','false','true','OVERTIMEEARNINGS','bab7b14c-c32f-4fa2-8656-4f580a174cec','RATEPERUNIT'),
	(18,'161 - Unpaid R&R Leave Taken','477','Hours','false','false','ORDINARYTIMEEARNINGS','600bd18d-fadb-400a-8aed-3048091f4a4e','RATEPERUNIT'),
	(19,'170 - Afternoon Shift','477','Hours','false','false','ORDINARYTIMEEARNINGS','1aa7a22d-8701-45e5-abea-e0700c6be5e2','RATEPERUNIT'),
	(20,'170 - Afternoon Shift (OT 1.5)','477','Hours','false','true','OVERTIMEEARNINGS','a79bbe42-2ff7-45d3-a953-6f7b330bddc5','RATEPERUNIT'),
	(21,'170 - Afternoon Shift (OT 2)','477','hours','false','true','OVERTIMEEARNINGS','8ae1e5b5-d155-4649-ad2c-239835b60575','RATEPERUNIT'),
	(22,'178 - Night Shift','477','hours','false','false','ORDINARYTIMEEARNINGS','2c28371e-fa32-42b8-b1c6-8dba858f0c4f','RATEPERUNIT'),
	(23,'178 - Night Shift - Adjustment','477','hours','false','false','ORDINARYTIMEEARNINGS','f9c936ff-f569-4bff-b3d7-319c43d04c91','RATEPERUNIT'),
	(24,'178 - Night Shift (OH 1.5)','477','hours','false','true','OVERTIMEEARNINGS','952a3c27-aae4-43e0-ba81-6cc408b41547','RATEPERUNIT'),
	(25,'178 - Night Shift (x1.5) - Adjustment','477','hours','false','true','OVERTIMEEARNINGS','c348010d-70c9-40fc-81d4-5240b159a27d','RATEPERUNIT'),
	(26,'178 - Night Shift (x2)','477','hours','false','true','OVERTIMEEARNINGS','ef76f80d-4176-45a2-8204-dcde4bc6a75c','RATEPERUNIT'),
	(27,'178 - Night Shift (x2) - Adjustment','477','hours','false','true','OVERTIMEEARNINGS','0930f958-f0c1-4ff6-9c52-d306188adecf','RATEPERUNIT'),
	(28,'Authorized LWOP (Unpaid)','477','hours','true','true','ORDINARYTIMEEARNINGS','e545f641-3d11-4b7b-b0f1-5fb022d9320c','RATEPERUNIT'),
	(29,'Fly-in/Fly-out','477','hours','false','false','ALLOWANCE','ab046c65-e6c1-4da8-b54b-65ab8e4aeca7','RATEPERUNIT'),
	(30,'Fly-in/Fly-out - Adjustment','477','hours','false','false','ALLOWANCE','7ba52f64-cc74-4238-b34e-7e8322c73dfe','RATEPERUNIT'),
	(31,'Holiday Leave Loading','477',NULL,'false','true','OVERTIMEEARNINGS','cc820179-685f-4f7c-b3ab-317add5386cb','MULTIPLE'),
	(32,'Leading Hand Allowance 1.5 - Adjustment','477','hours','false','true','ALLOWANCE','e2bd123e-1c33-4105-b82f-5d16c34af350','RATEPERUNIT'),
	(33,'Leading Hand Allowance 1.5 - DS','477','hours','false','true','ALLOWANCE','9e0b931e-d45d-44d2-be3d-79a92fa32300','RATEPERUNIT'),
	(34,'Leading Hand Allowance 1.5 - NS','477','hours','false','true','ALLOWANCE','bd3a107f-307b-42fd-8b47-7e9054c9daca','RATEPERUNIT'),
	(35,'Leading Hand Allowance Arvo','477','hours','false','false','ALLOWANCE','0ef9c76c-6d4a-4228-97d6-ac3e29ec27c4','RATEPERUNIT'),
	(36,'Leading Hand Allowance DS','477','hours','false','false','ALLOWANCE','5316744c-4666-4be1-9b0d-244c0563ccd1','RATEPERUNIT'),
	(37,'Leading Hand Allowance DS - Adjustment','477','hours','false','false','ALLOWANCE','f7541142-fc32-476a-a16e-e0d2fd065c3c','RATEPERUNIT'),
	(38,'Leading Hand Allowance NS','477','hours','false','false','ALLOWANCE','c46acfcf-85ed-4ba4-8674-789ec8fa087e','RATEPERUNIT'),
	(39,'Leading Hand Allowance PH','477','hours','false','false','ALLOWANCE','be8f8ef1-f677-4a0d-8acd-1e6324d22267','RATEPERUNIT'),
	(40,'Leading Hand Allowance PH - Adjustment','477','hours','false','false','ALLOWANCE','6c5a6914-1cfc-46f3-9b74-128136de96cf','RATEPERUNIT'),
	(41,'Retention Incentive','477',NULL,'false','false','ALLOWANCE','cba5af98-03bd-4856-9ef1-40f2bb7be4aa','FIXEDAMOUNT'),
	(42,'Site Allowance','477','hours','false','false','ALLOWANCE','7c8a0af9-de6f-4ee3-b25e-9156e0d792f3','RATEPERUNIT'),
	(43,'Site Allowance - Adjustment','477','hours','false','false','ALLOWANCE','6f5f5c8a-a0d7-42e6-bf7f-05073343ba52','RATEPERUNIT'),
	(44,'Site Allowance 1.5 - Adjustment','477','hours','false','true','ALLOWANCE','64abc1ec-0f70-4ca2-bb23-96127f237227','RATEPERUNIT'),
	(45,'Site Allowance 1.5 - DS','477','hours','false','true','ALLOWANCE','1b8cc089-d66f-42a6-a640-e1caf8aed9e9','RATEPERUNIT'),
	(46,'Site Allowance 1.5 - NS','477','hours','false','true','ALLOWANCE','f9488a24-1b96-4431-8c1e-d1c7684641ad','RATEPERUNIT'),
	(47,'Site Allowance 2 - Adjustment','477','hours','false','true','ALLOWANCE','280bffe9-ac60-4978-94d7-ab06ff56c0b4','RATEPERUNIT'),
	(48,'Site Allowance 2 - DS','477','hours','false','true','ALLOWANCE','80a354aa-bdd4-4472-906c-07227d390f8d','RATEPERUNIT'),
	(49,'Site Allowance 2 - DS Adjustment','477','hours','false','true','ALLOWANCE','bb056277-67da-4d55-9db6-79153efcf5c2','RATEPERUNIT'),
	(50,'Site Allowance 2 - NS','477','hours','false','true','ALLOWANCE','678b4682-f217-4882-a9b8-2910371ed09f','RATEPERUNIT'),
	(51,'Site Allowance 2 - NS Adjustment','477','hours','false','true','ALLOWANCE','bb7651ba-a1c3-42e2-9758-b76297401f7e','RATEPERUNIT'),
	(52,'Site Allowance Arvo','477','hours','false','false','ALLOWANCE','684c8859-a7c2-456c-aa27-019735c5db75','RATEPERUNIT'),
	(53,'Site Allowance DS','477','hours','false','false','ALLOWANCE','bad43636-f066-469c-a9da-e1186b2acf0b','RATEPERUNIT'),
	(54,'Site Allowance DS - Adjustment','477','hours','false','false','ALLOWANCE','9896d8f8-78ab-4d67-b5b5-5386ec19cbd6','RATEPERUNIT'),
	(55,'Site Allowance NS','477','hours','false','false','ALLOWANCE','fb0ff2e0-08a2-4ad8-a931-68a870916cf5','RATEPERUNIT'),
	(56,'Site Allowance PH','477','hours','false','false','ALLOWANCE','d24af763-de42-4513-8b16-23eef4db7b4e','RATEPERUNIT'),
	(57,'Site Allowance PH - Adjustment','477','hours','false','false','ALLOWANCE','5b45fcc4-26dc-4d76-9baf-ac023ca21776','RATEPERUNIT'),
	(58,'Leading Hand Allowance 2 - Adjustment','477','hours','false','true','ALLOWANCE','5b86645f-f70f-409d-acfa-0b4d24a45f95','RATEPERUNIT'),
	(59,'Leading Hand Allowance 2 - DS','461','hours','false','true','ALLOWANCE','23a833e5-9a76-4ffa-8526-a619dbfd0e5b','RATEPERUNIT'),
	(60,'Leading Hand Allowance 2 - NS','477','hours','false','true','ALLOWANCE','198bc851-d9e8-430a-885b-9fd1eaee8377','RATEPERUNIT'),
	(61,'Leading Hand Allowance 2 - NS Adjustment','477','hours','false','true','ALLOWANCE','9951d6c8-93bc-401c-9801-4694750bd704','RATEPERUNIT'),
	(62,'100 - Hours Worked (OH 1.5)','477','hours','false','false','ORDINARYTIMEEARNINGS','be2199a8-4874-4175-8772-7dabb44bcb7b','RATEPERUNIT'),
	(63,'100 - Hours Worked (OH 2)','477','hours','false','false','ORDINARYTIMEEARNINGS','447669bb-eb53-4314-82fe-c584f6228206','RATEPERUNIT'),
	(64,'170 - Afternoon Shift (OH 1.5)','477','hours','false','false','ORDINARYTIMEEARNINGS','e5fbb7fb-6efb-4a7e-9908-f14f7bf6f958','RATEPERUNIT'),
	(65,'170 - Afternoon Shift (OH 2)','477','hours','false','false','ORDINARYTIMEEARNINGS','553d5f52-09dd-49e7-9c5a-794806c32ea7','RATEPERUNIT'),
	(66,'Site Allowance 1.5 DS (OH)','477','hours','false','false','ALLOWANCE','466baa04-dbab-444f-bb1b-90fd79b83e1a','RATEPERUNIT'),
	(67,'Site Allowance 1.5 DS (OT)','477','hours','false','true','ALLOWANCE','9f60ce69-2d32-455e-b1e4-b721b3e59a69','RATEPERUNIT'),
	(68,'Site Allowance 2 NS (OT)','477','Hours','false','false','ALLOWANCE','45037ec3-30e6-464c-95bf-32179635110a','RATEPERUNIT'),
	(69,'Site Allowance 2 DS (OH)','477','hours','false','false','ALLOWANCE','ea5af798-5302-40c5-8f0f-1135e1520d81','RATEPERUNIT'),
	(70,'Site Allowance 2 DS (OT)','477','hours','false','true','ALLOWANCE','6c555222-ba7b-4436-86e1-bb1532e7aab4','RATEPERUNIT'),
	(71,'100 - Hours Worked (x2)','477','Hours','false','true','OVERTIMEEARNINGS','e68505e0-ae39-464e-a9d5-1cabf5aaf2ca','RATEPERUNIT');

/*!40000 ALTER TABLE `bkc_earning_rates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bkc_employees
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bkc_employees`;

CREATE TABLE `bkc_employees` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `DateOfBirth` datetime DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `MiddleNames` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Gender` varchar(100) DEFAULT NULL,
  `Mobile` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `TwitterUserName` varchar(255) DEFAULT NULL,
  `IsAuthorisedToApproveLeave` tinyint(1) DEFAULT NULL,
  `IsAuthorisedToApproveTimesheets` tinyint(1) DEFAULT NULL,
  `Occupation` varchar(255) DEFAULT NULL,
  `JobTitle` varchar(255) DEFAULT NULL,
  `Classification` varchar(255) DEFAULT NULL,
  `OrdinaryEarningsRateID` varchar(255) DEFAULT NULL,
  `PayrollCalendarID` varchar(255) DEFAULT NULL,
  `EmployeeGroupName` varchar(255) DEFAULT NULL,
  `BankAccounts` text,
  `PayTemplate` text,
  `OpeningBalances` text,
  `LeaveBalances` text,
  `SuperMemberships` text,
  `TerminationDate` datetime DEFAULT NULL,
  `EmployeeID` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `UpdatedDateUTC` timestamp NULL DEFAULT NULL,
  `TaxDeclaration` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `bkc_employees` WRITE;
/*!40000 ALTER TABLE `bkc_employees` DISABLE KEYS */;

INSERT INTO `bkc_employees` (`Id`, `FirstName`, `LastName`, `DateOfBirth`, `StartDate`, `Title`, `MiddleNames`, `Email`, `Gender`, `Mobile`, `Phone`, `TwitterUserName`, `IsAuthorisedToApproveLeave`, `IsAuthorisedToApproveTimesheets`, `Occupation`, `JobTitle`, `Classification`, `OrdinaryEarningsRateID`, `PayrollCalendarID`, `EmployeeGroupName`, `BankAccounts`, `PayTemplate`, `OpeningBalances`, `LeaveBalances`, `SuperMemberships`, `TerminationDate`, `EmployeeID`, `Status`, `UpdatedDateUTC`, `TaxDeclaration`)
VALUES
	(104,'Austin','Powers','1975-08-05 00:00:00','2019-01-08 00:00:00',NULL,NULL,'aaron.cuerden@gmail.com',NULL,NULL,'0455 267 022 ',NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','34bd182c-e05b-4970-ba78-e16af854ac0b','MORANBAH NORTH',NULL,NULL,NULL,NULL,NULL,NULL,'e5b29c3a-3ceb-40f4-8685-fb2dbaf80478','ACTIVE','2019-01-11 02:58:23',NULL),
	(105,'Donald','Duck','1997-08-03 00:00:00','2019-01-08 00:00:00',NULL,NULL,'tamey@bigpond.com',NULL,NULL,'046 618 6210',NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','34bd182c-e05b-4970-ba78-e16af854ac0b','MORANBAH NORTH',NULL,NULL,NULL,NULL,NULL,NULL,'4516636a-602c-4d22-a4ab-27cce392e45c','ACTIVE','2019-02-10 08:03:18',NULL),
	(106,'Dummy 1','Carra','1977-07-06 00:00:00','2018-08-24 00:00:00',NULL,NULL,'Dummy_A@carra.com.au','F',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','f52b8ca3-4784-40a5-abe0-dce4960387e1','CARRAPATEENA',NULL,NULL,NULL,NULL,NULL,NULL,'f0141af5-5619-4280-ab52-c45599277cb2','ACTIVE','2019-03-12 02:43:49',NULL),
	(107,'Dummy 10','Carra','1977-01-31 00:00:00','2018-09-21 00:00:00',NULL,NULL,'Dummy_J@carra.com.au','M',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','f52b8ca3-4784-40a5-abe0-dce4960387e1','CARRAPATEENA',NULL,NULL,NULL,NULL,NULL,NULL,'6bcf8b52-d0e8-430a-9a54-e2bc6663d71e','ACTIVE','2018-12-21 05:15:38',NULL),
	(108,'Dummy 11','Carra','1988-12-07 00:00:00','2018-10-09 00:00:00',NULL,NULL,'Dummy_K@carra.com.au','M',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','f52b8ca3-4784-40a5-abe0-dce4960387e1','CARRAPATEENA',NULL,NULL,NULL,NULL,NULL,NULL,'5746b2cd-38c2-48fd-9106-dd856f968605','ACTIVE','2018-12-21 05:18:16',NULL),
	(109,'Dummy 2','Carra','1987-08-10 00:00:00','2018-11-27 00:00:00',NULL,NULL,'Dummy_B@carra.com.au','M',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','f52b8ca3-4784-40a5-abe0-dce4960387e1','CARRAPATEENA',NULL,NULL,NULL,NULL,NULL,NULL,'ad117c7b-6dbb-4079-a49c-d3d5e29bd8b0','ACTIVE','2018-12-20 06:20:06',NULL),
	(110,'Dummy 3','Carra','1956-11-11 00:00:00','2018-12-04 00:00:00',NULL,NULL,'Dummy_C@carra.com.au','M',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','f52b8ca3-4784-40a5-abe0-dce4960387e1','CARRAPATEENA',NULL,NULL,NULL,NULL,NULL,NULL,'79212d62-50f7-4ea3-b311-25933401b2ad','ACTIVE','2018-12-21 03:14:55',NULL),
	(111,'Dummy 4','Carra','1984-03-02 00:00:00','2018-09-07 00:00:00',NULL,NULL,'Dummy_D@carra.com.au','M',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','f52b8ca3-4784-40a5-abe0-dce4960387e1','CARRAPATEENA',NULL,NULL,NULL,NULL,NULL,NULL,'ec5420a0-9d28-4c93-99d0-ab547ac4bda2','ACTIVE','2018-12-21 03:26:59',NULL),
	(112,'Dummy 5','Carra','1970-07-19 00:00:00','2018-07-19 00:00:00',NULL,NULL,'Dummy_E@carra.com.au','M',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','f52b8ca3-4784-40a5-abe0-dce4960387e1','CARRAPATEENA',NULL,NULL,NULL,NULL,NULL,NULL,'f01032cc-2ae0-415b-a786-f9cc5d960c15','ACTIVE','2018-12-21 03:35:56',NULL),
	(113,'Dummy 6','Carra','1967-11-04 00:00:00','2018-07-13 00:00:00',NULL,NULL,'Dummy_F@carra.com.au','M',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','f52b8ca3-4784-40a5-abe0-dce4960387e1','CARRAPATEENA',NULL,NULL,NULL,NULL,NULL,NULL,'d43ad5e5-4993-407f-95ad-d6ee7696d0c3','ACTIVE','2018-12-21 03:49:29',NULL),
	(114,'Dummy 7','Carra','1957-10-04 00:00:00','2018-07-20 00:00:00',NULL,NULL,'Dummy_G@carra.com.au','M',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','f52b8ca3-4784-40a5-abe0-dce4960387e1','CARRAPATEENA',NULL,NULL,NULL,NULL,NULL,NULL,'b1e47ac0-2f22-414a-8daf-06986e539f83','ACTIVE','2018-12-21 03:58:40',NULL),
	(115,'Dummy 8','Carra','1974-05-22 00:00:00','2019-01-08 00:00:00',NULL,NULL,'Dummy_H@carra.com.au','M',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','f52b8ca3-4784-40a5-abe0-dce4960387e1','CARRAPATEENA',NULL,NULL,NULL,NULL,NULL,NULL,'5b636aa3-8a45-427b-a587-cf08f5baf40c','ACTIVE','2018-12-21 04:14:24',NULL),
	(116,'Dummy 9','Carra','1988-07-25 00:00:00','2018-07-06 00:00:00',NULL,NULL,'Dummy_I@carra.com.au','M',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','f52b8ca3-4784-40a5-abe0-dce4960387e1','CARRAPATEENA',NULL,NULL,NULL,NULL,NULL,NULL,'d7f1c7b4-32be-46ec-a251-584d2f5ce52f','ACTIVE','2018-12-21 05:05:26',NULL),
	(117,'Dummy A','Amrun','1990-07-12 00:00:00','2018-09-26 00:00:00',NULL,'','Dummy_A@amrun.com.au','M',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','a227464b-30be-42c5-bd38-afe101d92abc','AMRUN',NULL,NULL,NULL,NULL,NULL,NULL,'2e38d974-87b8-425a-8f5b-4b3562c4e401','ACTIVE','2018-12-20 06:20:14',NULL),
	(118,'Dummy B','Amrun','1967-10-01 00:00:00','2018-05-31 00:00:00',NULL,'','Dummy_B@amrun.com.au','M',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','a227464b-30be-42c5-bd38-afe101d92abc','AMRUN',NULL,NULL,NULL,NULL,NULL,NULL,'92eaf694-b445-4214-8ed7-4728d927f51d','ACTIVE','2018-12-20 06:20:22',NULL),
	(119,'Dummy C','Amrun','1966-01-06 00:00:00','2018-09-06 00:00:00',NULL,'','Dummy_C@amrun.com.au','M',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','a227464b-30be-42c5-bd38-afe101d92abc','AMRUN',NULL,NULL,NULL,NULL,NULL,NULL,'1d1e93c4-2b73-40bb-ab60-d46a0efb8834','ACTIVE','2018-12-21 02:47:52',NULL),
	(120,'Dummy D','Amrun','1970-08-23 00:00:00','2018-07-18 00:00:00',NULL,'','Dummy_D@amrun.com.au','M',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','a227464b-30be-42c5-bd38-afe101d92abc','AMRUN',NULL,NULL,NULL,NULL,NULL,NULL,'8582ada9-7ea8-497c-b3fa-ace6e88c7c07','ACTIVE','2018-12-21 03:23:47',NULL),
	(121,'Dummy E','Amrun','1964-12-05 00:00:00','2018-07-04 00:00:00',NULL,'','Dummy_E@amrun.com.au','M',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','a227464b-30be-42c5-bd38-afe101d92abc','AMRUN',NULL,NULL,NULL,NULL,NULL,NULL,'804f0d1d-7223-4ccb-a6a6-d8bf609608cb','ACTIVE','2018-12-21 03:32:41',NULL),
	(122,'Dummy F','Amrun','1972-06-11 00:00:00','2018-07-25 00:00:00',NULL,'','Dummy_F@amrun.com.au','M',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','a227464b-30be-42c5-bd38-afe101d92abc','AMRUN',NULL,NULL,NULL,NULL,NULL,NULL,'babb1bb4-d190-46d7-be7d-cf3108a7795a','ACTIVE','2018-12-21 03:42:56',NULL),
	(123,'Dummy G','Amrun','1979-12-16 00:00:00','2018-09-13 00:00:00',NULL,'','Dummy_G@amrun.com.au','M',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','a227464b-30be-42c5-bd38-afe101d92abc','AMRUN',NULL,NULL,NULL,NULL,NULL,NULL,'c60e6a52-9a37-4c33-b0c8-1e76a59fdb3a','ACTIVE','2018-12-21 03:55:08',NULL),
	(124,'Dummy H','Amrun','1969-10-22 00:00:00','2018-06-20 00:00:00',NULL,'','Dummy_H@amrun.com.au','M',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','a227464b-30be-42c5-bd38-afe101d92abc','AMRUN',NULL,NULL,NULL,NULL,NULL,NULL,'867897f4-6fdc-4527-af18-d195ca463541','ACTIVE','2018-12-21 04:08:28',NULL),
	(125,'Dummy I','Amrun','1957-09-03 00:00:00','2018-05-31 00:00:00',NULL,'','Dummy_I@amrun.com.au','M',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','a227464b-30be-42c5-bd38-afe101d92abc','AMRUN',NULL,NULL,NULL,NULL,NULL,NULL,'d8585d07-8771-4a99-bfd9-dd2ca429e98c','ACTIVE','2018-12-21 04:42:34',NULL),
	(126,'Dummy J','Amrun','1974-11-14 00:00:00','2018-10-24 00:00:00',NULL,'','Dummy_J@amrun.com.au','M',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','a227464b-30be-42c5-bd38-afe101d92abc','AMRUN',NULL,NULL,NULL,NULL,NULL,NULL,'599bf1ff-9c4d-43fb-9375-62d2cd037d6d','ACTIVE','2018-12-21 05:09:35',NULL),
	(127,'Happy ','Gilmore','1996-12-17 00:00:00','2019-01-01 00:00:00',NULL,NULL,'peters1@live.com.au',NULL,NULL,'0409 757 217',NULL,NULL,NULL,NULL,NULL,NULL,'d171742d-192c-448c-9e99-edfd1a80a689','34bd182c-e05b-4970-ba78-e16af854ac0b','MORANBAH NORTH',NULL,NULL,NULL,NULL,NULL,NULL,'47351f6a-30b9-401e-8eac-142dde1e2cdb','ACTIVE','2019-01-11 03:01:59',NULL);

/*!40000 ALTER TABLE `bkc_employees` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bkc_mappings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bkc_mappings`;

CREATE TABLE `bkc_mappings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `table_key` varchar(255) DEFAULT NULL,
  `excel_column` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `required` tinyint(1) DEFAULT '0',
  `is_rate` tinyint(11) DEFAULT NULL,
  `rate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `bkc_mappings` WRITE;
/*!40000 ALTER TABLE `bkc_mappings` DISABLE KEYS */;

INSERT INTO `bkc_mappings` (`id`, `label`, `table_key`, `excel_column`, `type`, `required`, `is_rate`, `rate_id`)
VALUES
	(1,'Name','name','Name','text',0,0,NULL),
	(2,'Email','email','Email','email',1,0,NULL),
	(3,'Date','date','Date','date',1,0,NULL),
	(4,'100 - Hours Worked','rate1','1','float',0,1,1),
	(5,'100 - Hours Worked (x1.5)','rate2','2','float',0,1,7),
	(6,'100 - Hours Worked (x2)','rate3','3','float',0,1,71),
	(7,'170 - Afternoon Shift','rate4','4','float',0,1,19),
	(8,'170 - Afternoon Shift (x1.5)','rate5','5','float',0,1,20),
	(9,'170 - Afternoon Shift (x2)','rate6','6','float',0,1,21),
	(10,'178 - Night Shift','rate7','7','float',0,1,22),
	(11,'178 - Night Shift (x1.5)','rate8','8','float',0,1,25),
	(12,'178 - Night Shift (x2)','rate9','9','float',0,1,26),
	(13,'156 - Inclement Weather (Paid)','rate10','10','float',0,1,13),
	(14,'156 - Inclement Weather (Paid) x1.5','rate11','11','float',0,1,15),
	(15,'156 - Inclement Weather (Paid) x2','rate12','12','float',0,1,16),
	(16,'155 - Inclement Weather (Unpaid)','rate13','13','float',0,1,12),
	(17,'120 - Public Holiday Worked','rate14','14','float',0,1,10),
	(18,'110 - Public Holiday Not Worked','rate15','15','float',0,1,9);

/*!40000 ALTER TABLE `bkc_mappings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bkc_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bkc_permissions`;

CREATE TABLE `bkc_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table bkc_role_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bkc_role_permissions`;

CREATE TABLE `bkc_role_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `permission_id` (`permission_id`),
  CONSTRAINT `bkc_role_permissions_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `bkc_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `bkc_role_permissions_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `bkc_permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table bkc_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bkc_roles`;

CREATE TABLE `bkc_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `bkc_roles` WRITE;
/*!40000 ALTER TABLE `bkc_roles` DISABLE KEYS */;

INSERT INTO `bkc_roles` (`id`, `name`, `description`)
VALUES
	(1,'Superadmin','Administration');

/*!40000 ALTER TABLE `bkc_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bkc_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bkc_settings`;

CREATE TABLE `bkc_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` text,
  `value` text,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table bkc_timesheets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bkc_timesheets`;

CREATE TABLE `bkc_timesheets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `rate1` float DEFAULT '0',
  `rate2` float DEFAULT '0',
  `rate3` float DEFAULT '0',
  `rate4` float DEFAULT '0',
  `rate5` float DEFAULT '0',
  `rate6` float DEFAULT '0',
  `rate7` float DEFAULT '0',
  `rate8` float DEFAULT '0',
  `rate9` float DEFAULT '0',
  `rate10` float DEFAULT '0',
  `rate11` float DEFAULT '0',
  `rate12` float DEFAULT '0',
  `rate13` float DEFAULT '0',
  `rate14` float DEFAULT '0',
  `rate15` float DEFAULT '0',
  `upload_date` datetime DEFAULT NULL,
  `processed` tinyint(1) DEFAULT '0',
  `import_date` datetime DEFAULT NULL,
  `import_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table bkc_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bkc_users`;

CREATE TABLE `bkc_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` text,
  `password` text,
  `salt` text,
  `token` text,
  `push_notification_token` text,
  `device_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=Android,2=iOS,0=Not Defined',
  `verification_code` text,
  `last_login` datetime DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '0=inactive,1=active,2=email verified',
  `status_changed_date` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lsf user role` (`role_id`),
  CONSTRAINT `lsf user role` FOREIGN KEY (`role_id`) REFERENCES `bkc_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `bkc_users` WRITE;
/*!40000 ALTER TABLE `bkc_users` DISABLE KEYS */;

INSERT INTO `bkc_users` (`id`, `role_id`, `name`, `username`, `email`, `password`, `salt`, `token`, `push_notification_token`, `device_type`, `verification_code`, `last_login`, `avatar`, `status`, `status_changed_date`, `created_date`, `updated_date`)
VALUES
	(1,1,'Superadmin','superadmin','sujeet@pegotec.net','21232F297A57A5A743894A0E4A801FC3',NULL,'3368fdb4e9846edb88b9c54e4d64efb3','',1,NULL,'2019-03-17 11:15:38','',1,NULL,'2018-10-25 17:20:20','2018-10-25 17:20:23');

/*!40000 ALTER TABLE `bkc_users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
