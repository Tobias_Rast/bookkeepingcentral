<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit8443a3e647f4cf728f7fd7b139b7c876
{
    public static $prefixLengthsPsr4 = array (
        'X' => 
        array (
            'XeroPHP\\' => 8,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'XeroPHP\\' => 
        array (
            0 => __DIR__ . '/..' . '/calcinai/xero-php/src/XeroPHP',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit8443a3e647f4cf728f7fd7b139b7c876::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit8443a3e647f4cf728f7fd7b139b7c876::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
